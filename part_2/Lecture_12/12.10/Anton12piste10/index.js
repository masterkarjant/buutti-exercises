module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    console.log("query", req.query);
    console.log("param", req.params);

    let result = "";
    if (req.query) {
        try {
            let minNum = parseFloat(req.query.min);
            let maxNum = parseFloat(req.query.max);
            let dec = (req.query.dec == "true" || req.query.dec == "false") ? "ok" : null;

            if (!dec || isNaN(minNum) || isNaN(maxNum) || minNum > maxNum)
                throw 'nope'

            const randomnum = req.query.dec == "true"
                ? (Math.random() * (maxNum - minNum) + minNum).toFixed(2)
                : Math.floor(Math.random() * (maxNum - minNum) + minNum);

            result = randomnum;
        }
        catch (e) {
            console.log(e)
            result = "invalid parameters";
        }
    }

    context.res = {
        status: 200, body: `
        Random Number Producer
        ----
        How to use:
        To your url request add params min, max, and dec
        for example like this:
        (http://localhost:3000/?min=10&max=20&dec=true)
        min for minimum number
        max for maximum number
        dec for if you want the result be a decimal number or not, use true or false.
        ------
        result: ${result}
        `
    };
}