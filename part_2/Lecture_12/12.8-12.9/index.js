import express from 'express';

const server = express();

server.get('/', (req, res) => {

    console.log("query", req.query);
    console.log("param", req.params);

    let result = "";
    if (req.query)

        try {
            let minNum = parseFloat(req.query.min);
            let maxNum = parseFloat(req.query.max);
            let dec = (req.query.dec == "true" || req.query.dec == "false") ? "ok" : null;

            if (!dec || isNaN(minNum) || isNaN(maxNum) || minNum > maxNum)
                throw 'nope'

            const randomnum = req.query.dec == "true"
                ? (Math.random() * (maxNum - minNum) + minNum).toFixed(2)
                : Math.floor(Math.random() * (maxNum - minNum) + minNum);

            result = "<h4>" + randomnum + "</h4>";
        }
        catch (e) {
            console.log(e)
            result = "<h4>invalid parameters</h4>";
        }

    res.send(`
    <h1>Random Number Producer</h1>
    <h4>How to use:</h4>
    to your url request add params min, max, and dec<br> 
    for example like this <b>(http://localhost:3000/?min=10&max=20&dec=true)</b> <br>
    <b>min</b> for minimum number<br>
    <b>max</b> for maximum number<br>
    <b>dec</b> for if you want the result be a decimal number or not, use true or false.

    ${result}
    `)
})

const PORT = process.env.PORT || 3000
server.listen(PORT, () => {
 console.log('Server listening to port', PORT)
})