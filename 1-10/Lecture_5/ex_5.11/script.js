


const toggleFormBtn = document.querySelector("#toggleFormBtn");
const form = document.querySelector("#form");
const postName = document.querySelector("#name");
const postText = document.querySelector("#text");

const postsArr = [];

function toggleForm() {
    if (toggleFormBtn.innerHTML == "Add new post") {
        toggleFormBtn.innerHTML = "Hide form";
        form.style.display = "block";
    }
    else {
        toggleFormBtn.innerHTML = "Add new post";
        form.style.display = "none";
    }
}

let postCount = 0;
function submitForm(event) {
    // log.textContent = `Form Submitted! Time stamp: ${event.timeStamp}`;
    event.preventDefault();
    console.log(postName.value);
    console.log(postText.value);

    postCount++;
    postsArr.push({ name: postName.value, text: postText.value, id: postCount });


    const forumDoc = document.getElementById("forum");
    const div = document.createElement("div");
    forumDoc.appendChild(div);
    div.className = "post";
    div.id = `postId${postCount}`;
    let post = `
    <h1>${postName.value || "post id" + postCount}</h1>
    <p>${postText.value || "post id" + postCount}</p>
    <span><button onclick="deletePost(${postCount})">Delete</button></span>`;
    div.innerHTML = post;
    postName.value = "";
    postText.value = "";
}

function deletePost(targetId) {
    console.log(targetId);
    const targetPost = document.getElementById(`postId${targetId}`);
    targetPost.remove();
    postsArr.splice(postsArr.findIndex(x => x.id == targetId), 1);

    console.log(postsArr);

}

form.addEventListener("submit", submitForm);