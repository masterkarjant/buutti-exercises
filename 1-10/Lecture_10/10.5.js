import "dotenv/config";
import jwt from "jsonwebtoken";

const payload = { username: "jediknight" };
const secret = "process.env.SECRET";
const options = { expiresIn: "15m"};

const token = jwt.sign(payload, secret, options);
console.log(token);