import { server } from "./server.js";
import request from "supertest";


describe("Server Tests", () => {
    it("Test unknownEndpoint", async () => {
        const response = await request(server)
            .get("/invalidaddress");
        expect(response.statusCode).toBe(404);
    });

    it("Returns 401 on failed login", async () => {
        const response = await request(server)
            .post("/login")
            .send({ username: "jedi", password: "knight" });

        expect(response.statusCode).toBe(401);
    });

    it("Is not able to register with insufficient data", async () => {
        const response = await request(server)
            .post("/register")
            .send({ username: "jedi" });

        expect(response.statusCode).toBe(404);
    });

    it("Is able to register", async () => {
        const response = await request(server)
            .post("/register")
            .send({ username: "jedi", password: "knight" });

        expect(response.statusCode).toBe(201);
    });

    it("Is able to login after registration", async () => {
        const response = await request(server)
            .post("/login")
            .send({ username: "jedi", password: "knight" });

        expect(response.statusCode).toBe(200);
    });
});
