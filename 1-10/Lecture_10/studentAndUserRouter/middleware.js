import jwt from "jsonwebtoken";

// MIDDLEWARE
export const logger = (req, _res, next) => {
    const date = new Date();
    console.log("req made at:", date.toLocaleString());
    console.log("req method:", req.method);
    console.log("req url endpoint:", req.url);
    if (Object.keys(req.body).length > 0)
        console.log("req body:", req.body);

    next();
};
export const unknownEndpoint = (_req, res, _next) => {
    res.status(404).send({ error: "Error 404, Unknown Endpoint" });
};

export const authenticate = (req, res, next) => {
    let userData;
    try {
        const header = req.get("Authorization");
        const token = header.substring(7);
        userData = jwt.verify(token, process.env.SECRET);
    }
    catch (e) {
        return res.status(401).send();
    }

    next();
};

export const authenticateAdmin = (req, res, next) => {
    // let userData;
    // try {
    //     const header = req.get("Authorization");
    //     const token = header.substring(7);
    //     userData = jwt.verify(token, process.env.SECRET_ADMIN);
    // }
    // catch (e) {
    //     return res.status(401).send();
    // }

    next();
};