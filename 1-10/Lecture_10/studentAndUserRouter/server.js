
import express from "express";
import userRouter from "./userRouter.js";
import studentRouter from "./studentRouter.js";
import { authenticate, logger, unknownEndpoint } from "./middleware.js";

export const server = express();

server.use(express.json());
server.use(express.static("public"));
// server.use(logger);

server.use("/", userRouter);
server.use("/student", authenticate, studentRouter);

server.use(unknownEndpoint);

server.listen(3050, () => {
    console.log("Listening to port 3050");
});