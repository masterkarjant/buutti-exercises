import { server } from "./server.js";
import request from "supertest";

const userToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImplZGkiLCJpYXQiOjE2NjkyODc5NDAsImV4cCI6MTY2OTI5NTE0MH0.FT1KCt1Ze-qaj32iHRjZIZJq0N2jhNBSvOM1-l9IH0U";
const adminToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjY5Mjg5OTQ0LCJleHAiOjE2NjkyOTcxNDR9._voVSdiYDdttLk44S4EsJW4FzdUXRvp1b4lmEUG24AU";

// ottaisin ^ nuo myös .env tiedostosta

describe("Server Tests as User", () => {

    it("Test unknownEndpoint", async () => {
        const response = await request(server)
            .get("/invalidaddress");
        expect(response.statusCode).toBe(404);
    });

    it("Returns 401 on failed login", async () => {
        const response = await request(server)
            .post("/api/v1/users/login")
            .send({ username: "jedi", password: "knight" });

        expect(response.statusCode).toBe(401);
    });

    it("Is not able to register with insufficient data", async () => {
        const response = await request(server)
            .post("/api/v1/users/register")
            .send({ username: "jedi" });

        expect(response.statusCode).toBe(404);
    });


    it("Is not able to POST any book without token", async () => {
        const response = await request(server)
            .post("/api/v1/books/")
            .send({ id: "1", name: "name", author: "boba", read: "ofc" });

        expect(response.statusCode).toBe(401);
    });

    it("Is able to register", async () => {
        const response = await request(server)
            .post("/api/v1/users/register")
            .send({ username: "jedi", password: "knight" });

        expect(response.statusCode).toBe(201);
    });

    it("Is able to login after registration", async () => {
        const response = await request(server)
            .post("/api/v1/users/login")
            .send({ username: "jedi", password: "knight" });

        expect(response.statusCode).toBe(200);
    });

    it("Is able to reach GET endpoint with userToken", async () => {
        const response = await request(server)
            .get("/api/v1/books/")
            .set("Authorization", `Bearer ${userToken}`)
            .send();

        expect(response.statusCode).toBe(200);
    });

    it("Is not able to POST any book with userToken", async () => {
        const response = await request(server)
            .post("/api/v1/books/")
            .send({ id: "1", name: "name", author: "boba", read: "ofc" })
            .set("Authorization", `Bearer ${userToken}`);

        expect(response.statusCode).toBe(401);
    });
});

describe("Server Tests as Admin", () => {

    it("Is able to POST any book with userToken", async () => {
        const response = await request(server)
            .post("/api/v1/books/")
            .set("Authorization", `Bearer ${adminToken}`)
            .send({ id: "1", name: "name", author: "boba", read: "ofc" });

        expect(response.statusCode).toBe(201);
    });

    it("Is able to reach PUT endpoint with adminToken and edit a book", async () => {
        const response = await request(server)
            .put("/api/v1/books/1")
            .set("Authorization", `Bearer ${adminToken}`)
            .send({ id: "2", name: "name is lost", author: "boba", read: "ofc" });

        expect(response.statusCode).toBe(204);
    });

    it("Is able to reach GET endpoint with adminToken", async () => {
        const response = await request(server)
            .get("/api/v1/books/")
            .set("Authorization", `Bearer ${adminToken}`)
            .send();

        expect(response.statusCode).toBe(200);
    });

    it("Is able to reach DELETE endpoint with adminToken", async () => {
        const response = await request(server)
            .get("/api/v1/books/2")
            .set("Authorization", `Bearer ${adminToken}`)
            .send();

        expect(response.statusCode).toBe(200);
    });
});
