
import express from "express";
import userRouter from "./userRouter.js";
import bookRouter from "./bookRouter.js";
import { unknownEndpoint, authenticate } from "./middleware.js";

export const server = express();

server.use(express.json());
// server.use(express.static("public"));
// server.use(logger);

server.use("/api/v1/users", userRouter);
server.use("/api/v1/books", authenticate, bookRouter);

server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});