import jwt from "jsonwebtoken";

// MIDDLEWARE
export const logger = (req, _res, next) => {
    const date = new Date();
    console.log("req made at:", date.toLocaleString());
    console.log("req method:", req.method);
    console.log("req url endpoint:", req.url);
    if (Object.keys(req.body).length > 0)
        console.log("req body:", req.body);

    next();
};

export const unknownEndpoint = (_req, res, _next) => {
    res.status(404).send({ error: "Error 404, Unknown Endpoint" });
};

export const authenticate = (req, res, next) => {
    let userData;
    try {
        const header = req.get("Authorization");
        const token = header.substring(7);
        userData = jwt.verify(token, "voidKey");
    }
    catch (e) {
        return res.status(401).send();
    }

    next();
};

export const authenticateAdmin = (req, res, next) => {
    let userData;
    try {
        const header = req.get("Authorization");
        const token = header.substring(7);
        userData = jwt.verify(token, "voidKey");

        userData.isAdmin ? next() : res.status(401).send();
    }
    catch (e) {
        return res.status(401).send();
    }
};

export const checkIfAllRequiredBodyDataIsPresentForBookPost = (req, res, next) => {
    if (req.body.id && req.body.name && req.body.author && req.body.read) {
        next();
    }
    else {
        res.status(404).send("insufficient data");
    }
};

