import argon2 from "argon2";
import express from "express";
import "dotenv/config";
import jwt from "jsonwebtoken";

const router = express.Router();

const usersObj = { admin: {} };

// POST
router.post("/register", async (req, res) => {
    if (usersObj[req.body.username])
        return res.status(409).send("Username taken");

    if (req.body.username && req.body.password) {
        const hash = await hashPassword(req.body.password);
        usersObj[req.body.username] = { username: req.body.username, passwordHash: hash };
        const token = createToken(req.body.username);
        res.status(201).send(token);
        console.log(usersObj, token);
    }
    else {
        res.status(404).send();
    }
});

router.post("/login", async (req, res) => {
    if (req.body.username && req.body.password && usersObj[req.body.username]) {

        let correctPassword;

        if (req.body.username === "admin")
            correctPassword = await verifyPassowrd(process.env.ADMIN_PASSWORD_HASH, req.body.password);
        else
            correctPassword = await verifyPassowrd(usersObj[req.body.username].passwordHash, req.body.password);

        if (!correctPassword)
            return res.status(401).send();

        const token = createToken(req.body.username);
        res.status(200).send(token);
    }
    else {
        res.status(401).send();
    }
});

router.post("/admin", async (req, res) => {
    if (req.body.username && req.body.password && process.env.ADMIN_USERNAME === req.body.username && process.env.ADMIN_PASSWORD_HASH) {
        const correctPassword = await verifyPassowrd(process.env.ADMIN_PASSWORD_HASH, req.body.password);

        console.log("inside if", process.env.ADMIN_USERNAME == req.body.username);

        correctPassword ? res.status(204).send() : res.status(401).send();
    }
    else {
        res.status(401).send();
    }
});

const hashPassword = async (password) => {
    const hash = await argon2.hash(password);
    return hash;
};

const verifyPassowrd = async (hash, password) => {
    const result = await argon2.verify(hash, password);
    return result;
};

const createToken = (username) => {
    let payload;
    username === "admin" ? payload = { username: username, isAdmin: true } : payload = { username: username };
    const secret = "voidKey";
    const options = { expiresIn: "2h" };
    const token = jwt.sign(payload, secret, options);
    return token;
};

export default router;
