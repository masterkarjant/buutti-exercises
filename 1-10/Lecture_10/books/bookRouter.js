import argon2 from "argon2";
import express from "express";
import { checkIfAllRequiredBodyDataIsPresentForBookPost, authenticateAdmin } from "./middleware.js";

const router = express.Router();

const books = [];

// GET
router.get("/", (_req, res) => {
    res.send(books);
});

router.get("/:id", (req, res) => {
    const bookIndex = books.findIndex(x => x.id === req.params.id);
    if (bookIndex > -1)
        res.send(books[bookIndex]);
    else {
        res.status(404).send();
    }
});

// POST
router.post("/", authenticateAdmin, checkIfAllRequiredBodyDataIsPresentForBookPost, (req, res) => {
    console.log("BODY", req.body);
    books.push({ id: req.body.id, name: req.body.name, author: req.body.author, read: req.body.read });
    res.status(201).send();
});

// // PUT
router.put("/:id", authenticateAdmin, (req, res) => {
    const bookIndex = books.findIndex(x => x.id === req.params.id);
    if (bookIndex > -1) {
        books[bookIndex] = { id: req.body.id || books[bookIndex].id, name: req.body.name || books[bookIndex].name, author: req.body.author || books[bookIndex].author, read: req.body.read || books[bookIndex].read };
        res.status(204).send();
    }
    else
        res.status(404).send();
});

// // DELETE
router.delete("/:id", authenticateAdmin, (req, res) => {
    const bookIndex = books.findIndex(x => x.id === req.params.id);
    if (bookIndex > -1) {
        books.splice(bookIndex, 1);
        res.send("Book removed from the books list");
    }
    else {
        res.status(404).send();
    }
});


export default router;