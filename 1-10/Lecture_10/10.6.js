import "dotenv/config";
import jwt from "jsonwebtoken";

// const payload = { username: "jediknight" };
// const secret = "process.env.SECRET";
// const options = { expiresIn: "15m" };

// const token = jwt.sign(payload, secret, options);
// console.log(token);

let tokenStuff;
try {
    tokenStuff = jwt.verify("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImplZGlrbmlnaHQiLCJzdGF0dXMiOiJoZXJvIG9mIGhlcm9lcyIsImlhdCI6MTY2OTIwMTQzMSwiZXhwIjoxNjY5MjAyMzMxfQ.GBuH9vWrSSCS9D4YE8ckVcNwTy5KUHzrivjBAQBM5WM", "process.env.SECRET");
    console.log(tokenStuff);
}
catch (e) {
    console.log(":-: token verification failed");
}
