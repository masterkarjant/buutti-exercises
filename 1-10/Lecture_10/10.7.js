import express from "express";
import "dotenv/config";
import jwt from "jsonwebtoken";

const server = express();

const authenticate = (req, res, next) => {
    const header = req.get("Authorization");
    console.log(header);
    const token = header.substring(7);
    const userData = jwt.verify(token, process.env.SECRET);
    console.log(userData);
    next();
};

server.get("/", authenticate, (req, res) => {
    res.send("ok");
});

server.listen(3000, () => {
    console.log("Listening to port 3000");
});