
// EX 4.12


// Open Terminal and type: node ex_4.12.js

/* TEHTÄVÄ

Define a number n that is larger than 0, for example n = 3
Create a function that given parameter n finds the number of steps it takes to reach number 1 (one) using the following process

If n is even, divide it by 2
If n is odd, multiply it by 3 and add 1

Example:
For n = 3 the process would be following
0: n0 = 3
1: 3 is odd, so we multiply by three and add one. n1 = n0 * 3 + 1 = 10
2: 10 is even, so we divide by two. n2 = n1 / 2 = 5
3: n3 = n2 * 3 + 1 = 16
4: n4 = n3 / 2 = 8
5: n5 = n4 / 2 = 4
6: n6 = n5 / 2 = 2
7: n7 = n6 / 2 = 1
So we reach n = 1 after seven steps.
You can read more about the Collatz conjecture from Wikipedia or XKCD.
*/

const number = 3;

// "method"? :D
const method = (number) => {
    let steps = 0;

    while (number > 1) {
        if (number % 2 == 0) // even
            number = number / 2;
        else // odd
            number = number * 3 + 1;

        steps++;
    }

    return steps;

};

console.log(method(number));

// [1,2,3,4,5,6,7,8,9,10].forEach(x => console.log(method(x)));
// Hyvin toimii!