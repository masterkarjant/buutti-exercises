
// EX 4.11


// Open Terminal and type: node ex_4.11.js

/* TEHTÄVÄ

Create a function (or multiple functions) that generates username and password from given firstname and lastname.
Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in lower case
Password:  1 random letter + first letter of first name in lowercase + last letter of last name in uppercase + random special character + last 2 numbers from current year
Get to know to ASCII table. Random letters and special characters can be searched from ASCII table with String.fromCharCode() method with indexes. For example String.fromCharCode(65) returns letter A.
Hints:
Generate random numbers (indexes) to get one random letter and one special character. Use range of 65 to 90 to get the (uppercase) LETTER and 33 to 47 to get the SPECIAL CHARACTER. Notice, that these are not the only "special characters", but using this range is acceptable for this exercise. Use build-in function to get the current year.
Links: http://www.asciitable.com/

generateCredentials('John', 'Doe') // 
*/

console.log(String.fromCharCode());

function generateCredentials(firstName, lastName) {
    const lastTwoNumbersOfCurrentYear = new Date().getFullYear().toString().substring(2);
    const randomSpecialChar = String.fromCharCode(Math.floor(Math.random() * (47 - 33) + 33));
    const randomChar = String.fromCharCode(Math.floor(Math.random() * (90 - 65) + 65));
    const password = randomChar + firstName[0].toLowerCase() + lastName[lastName.length - 1].toUpperCase() + randomSpecialChar + lastTwoNumbersOfCurrentYear;
    const username = "B" + new Date().getFullYear().toString().substring(2) + firstName.substring(0, 2).toLowerCase() + lastName.substring(0, 2).toLowerCase();
    return [username, password];
}

console.log(generateCredentials("John", "Doe"));

