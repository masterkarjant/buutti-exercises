
// EX 4.7


// Open Terminal and type: node ex_4.7.js

/* TEHTÄVÄ

Create a "like" function that takes in a array of names. Depending on a number of names (or length of the array) the function must return strings as follows:

likes([]); // "no one likes this"
likes(["John"]) // "John likes this"
likes(["Mary", "Alex"]) // "Mary and Alex like this"
likes(["John", "James", "Linda"]) // "John, James and Linda like this"
likes(["Alex", "Linda", "Mark", "Max"]) // must be "Alex, Linda and 2 others 


For 4 or more names, "2 others" simply increases.

Assignment 4.8: Above Average
*/

function whoLikesThis(arr) {
    let str = "";

    if (arr.length == 0)
        str = "No one likes this";
    else if (arr.length == 1)
        str = arr[0] + " likes this";
    else if (arr.length == 2)
        str = arr[0] + " and " + arr[1] + " likes this";
    else if (arr.length < 4)
        arr.forEach((name, i) => {
            str = str + name;
            if (i + 2 == arr.length) // means next one is last of the list
                str = str + " and ";
            else
                str = str + ", ";

            if (i + 1 == arr.length)
                str += "like this";
        });
    else
        str = `${arr[0]}, ${arr[1]} and ${arr.length - 2} others like this`;

    return str;
}
// Saattaisi olla selkeämpää, jos jokaisessa koodiblokissa olisi toisiaan vastaava toteutus.
// Nyt ensimmäisessä on tapaus 1, jossa on katenaatio, toisessa on tapaus 0, jossa on kovakoodattu string,
// kolmannessa blokissa käsitellään kaksi tapausta ja viimeisessä blokissa sitten loput string templatella
// Tämän seurauksena myöskin tapaus kaksi nimeä tuottaa bugin (ylimääräinen pilkku):
// Anton and Naudi, like this

// KORJATTU

console.log(whoLikesThis([]));
console.log(whoLikesThis(["Anton"]));
console.log(whoLikesThis(["Anton", "Naudi"]));
console.log(whoLikesThis(["Anton", "Naudi", "Elon"]));
console.log(whoLikesThis(["Anton", "Naudi", "Elon", "Mom"]));
console.log(whoLikesThis(["Anton", "Naudi", "Elon", "Mom", "Dad"]));