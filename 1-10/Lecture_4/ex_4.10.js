
// EX 4.10


// Open Terminal and type: node ex_4.10.js

/* TEHTÄVÄ

The first input array is the key to the correct answers to an exam, like ["a", "a", "b", "d"]. The second one contains a student's submitted answers.
The two arrays are not empty and are the same length. Return the score for this array of answers, giving +4 for each correct answer, -1 for each incorrect answer, and +0 for each blank answer, represented as an empty string.
If the score < 0, return 0
For example:

checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]) → 6  
checkExam(["a", "a", "c", "b"], ["a", "a", "b",  ""]) → 7  
checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]) → 16  
checkExam(["b", "c", "b", "a"], ["",  "a", "a", "c"]) → 0 
*/

const correctAnswers = ["c", "b", "b", "c", "d", "c"];

// Tehtävänannon mukaan checkExam funktion tulee ottaa kaksi parametria
function checkScore(submittedAnswers) {
    let totalScore = 0;
    submittedAnswers.forEach((answer, i) => {
        if (answer === correctAnswers[i]) totalScore = totalScore + 4;
        else if (answer === "") { // this is empty 
        }
        else totalScore = totalScore - 1;
    });

    return totalScore < 0 ? 0 : totalScore;

    // if (totalScore < 0)
    //     return 0;
    // else
    //     return totalScore;
    // Muista ternäärioperaattorin ( ... ? ... : ... ) käyttö näissä yksinkertaisissa tapauksissa
    // KORJATTU
}

console.log(checkScore(["c", "a", "c", "a", "a", "a"]));



