
// EX 4.8


// Open Terminal and type: node ex_4.8.js

/* TEHTÄVÄ

Create a function that takes in an array and returns a new array of values that are above average.

aboveAverage([1, 5, 9, 3]) // [5, 9]
*/

function returnAboveAverage(arr) {

    let sum = arr.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
    // forEach ei ole "looginen" summan laskemiseen, koska siinä tarvitaan ulkoista muuttujaa, jota mutatoidaan
    // reduce hoitaisi tämän yhdellä rivillä
    // const average = arr.reduce(...) / arr.length;
    const average = sum / arr.length;

    return arr.filter(x => x > average);

    // KORJATTU KÄYTTÄMÄÄN REDUCEA
}

console.log(returnAboveAverage([1, 5, 9, 3]));

