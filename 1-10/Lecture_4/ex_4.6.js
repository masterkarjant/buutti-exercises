
// EX 4.6


// Open Terminal and type: node ex_4.6.js

/* TEHTÄVÄ

Create a function that takes in an operator and two numbers and returns the result.

function calculator(operator, num1, num2) {
    // your code
}

calculator("+", 2, 3) // returns 5


For example, if the operator is "+", sum the given numbers. In addition to sum, calculator can also calculate differences, multiplications and divisions. If the operator is something else, return some error message like "Can't do that!"
*/

function calculator(operator, num1, num2) {
    let result;
    if (operator == "*")
        result = num1 * num2;
    else if (operator == "-")
        result = num1 - num2;
    else if (operator == "+")
        result = num1 + num2;
    else if (operator == "/")
        result = num1 / num2;
    else
        result = "Error: Can't do that!";

    return result;
}

console.log(calculator("*", 2, 10));