function Ingredient(name, amount) {
    this.name = name;
    this.amount = amount;
}

function Recipe(name, ingredients, servings) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
}

Recipe.prototype.setServings = function(newServings) {
    let newNewServings = newServings / this.servings;
    this.ingredients.forEach(element => {
        element.amount = element.amount * newNewServings;
    });
    this.servings = newServings;
};

Recipe.prototype.toString=function() {
        
    let tulos = "";
    tulos +="Name: "+this.name+"\n";
    tulos +="Servings: "+this.servings+"\n";
    
    tulos +="Recipe:\n";
    this.ingredients.forEach(element => {
        tulos += element.name +" "+element.amount+"\n";
    });
    
    return tulos;
};
Recipe.prototype.toString=function() {
        
    let tulos = "";
    tulos +="Name: "+this.name+"\n";
    tulos +="Servings: "+this.servings+"\n";
    
    tulos +="Recipe:\n";
    this.ingredients.forEach(element => {
        tulos += element.name +" "+element.amount+"\n";
    });
    
    return tulos;
};


const carrot = new Ingredient("Carrot",2);
Ingredient.prototype.scale = function(amount) {
    this.amount = this.amount * amount;
};
//carrot.scale(10);
const bacon = new Ingredient("Bacon",4);

const aamupala = new Recipe("Aamupala",[carrot, bacon],1);
aamupala.setServings(20);
console.log(aamupala.toString());
