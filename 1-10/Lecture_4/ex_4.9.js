
// EX 4.9


// Open Terminal and type: node ex_4.9.js

/* TEHTÄVÄ

Return the number (count) of vowels in the given string. Let's consider a, e, i, o, u, y as vowels in this exercise.

getVowelCount('abracadabra') // 5
*/

const vowels = ["a", "e", "i", "o", "u", "y"];
// vowels voisi myös olla string "aeiouy" mutta arrayssä ei ole mitään vikaa

function getVowelCount(word) {

    const vowelsInWord = word.split("").reduce((accumulator, currentValue) => vowels.includes(currentValue) ? accumulator + currentValue : accumulator, "");

    // console.log(count2);
    return vowelsInWord.length;
    // word.split("").forEach(c => {
    //     if (vowels.includes(c))
    //         count++;
    // });

    // return count;

    // KORJATTU Käyttämäänd reducea
}

console.log(getVowelCount("abracaudabraii"));

// Tässä tapauksessa forEach ei ole epälooginen kuten edellisessä tehtävässä,
// mutta reduce jälleen hoitaisi tämän melko selkeällä onelinerilla

