
// EX 2.11


/* TEHTÄVÄ

Create a new repository in GitLab and clone it using ssh key to your machine

Then, create a new README.md file with some text in it in the repository. (You can use code from earlier exercises you’ve done or are working on at the moment!)

Add it, commit it, and push it to Git. Go to GitLab and see that the file is there!
*/

