
// EX 2.13


// Open Terminal and type "node ex_2.13.js" to run the file, you can also add arguments eg. "node ex_2.13.js en" is the default, replacing en with fi or es will change the output

/* TEHTÄVÄ

Create a program that takes in one argument from command line, a language code (e.g. "fi", "es", "en"). Console.log "Hello World" for the given language for atleast three languages. It should default to console.log "Hello World".
Remember to test that the program outputs the right answer in all cases.

Hint: use process.argv for input
*/

const args = process.argv;

const language = args[2];

if (language == "en")
    console.log("Hello World");
else if (language == "fi")
    console.log("Hello World in Finnish");
else if (language == "es")
    console.log("Hello World in Spanish");
else if (!language)
    console.log("Hello World");
else
    console.log("Language not recognized, options include 'fi', 'en', 'es'");