
// EX 2.9


// Open Terminal and type "node ex_2.9.js" to run the file

/* TEHTÄVÄ
Print out the largest number found in this array. Do this without first sorting the array, or using any functions from the Math module.

Extra: Find the 2nd largest number in the array, again without sorting the array first, or using Math functions to aid you.
*/

const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];
let largest = arr[0];
let secondLargest = arr[0];

arr.forEach(num => {
    if (num > largest) {
        secondLargest = largest;
        largest = num;
    }
    if (num > secondLargest && num != largest)
        secondLargest = num;

});

console.log("largest", largest);
console.log("second largest", secondLargest);