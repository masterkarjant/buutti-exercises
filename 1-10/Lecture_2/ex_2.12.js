
// EX 2.12


/* TEHTÄVÄ

Create a new branch (with a name dev, for instance) in your new Git repository. Checkout the branch, make some changes to main.js there, and push the changes to GitLab.

Then, create merge request from dev branch to master branch by using gitlab button or remote url

Finally, approve the merge request in GitLab
*/
