
// EX 2.19


// Open Terminal and type "node ex_2.19.js" with additional modifiers lower/upper and a string. Eg. node ex_2.19.js lower "Do you LIKE Snowmen?"
/* TEHTÄVÄ

Create a program that takes in a string, and modifies the every letter of that string to upper case or lower case, depending on the input
example: node .\modifycase.js lower "Do you LIKE Snowmen?" -> do you like snowmen
example: node .\modifycase.js upper "Do you LIKE Snowmen?" -> DO YOU LIKE SNOWMEN
NOTE remember to take in the 2nd parameter with quotation marks
*/

const args = process.argv;
const modifier = args[2];
const text = args[3];

if (modifier == "upper") {
    console.log(text.toUpperCase());
}
else if (modifier == "lower") {
    console.log(text.toLowerCase());
}
else
    console.log("issue with the modifier. It can be either 'upper' or 'lower'");








