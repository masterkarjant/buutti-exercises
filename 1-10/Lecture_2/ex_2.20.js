
// EX 2.20


// Open Terminal and type "node ex_2.20.js" with 3 names eg node ex_2.20.js a ab abc

/* TEHTÄVÄ

Create a program that takes in a string and drops off the last word of any given string, and console.logs it out.
example: node .\annoyingSubstring.js "Hey I'm alive!" -> Hey I'm

1.14 Replace characters (difficult)
Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.
example: node .\replacecharacters.js g h "I have great grades for my grading" -> I have hreat hrades for my hrading

Hint: https://www.w3schools.com/jsref/jsref_replace.asp
*/

const args = process.argv;
const text = args[2].trim();

const targetCharToReplace = "a";
const replaceWithChar = "b";

console.log("Remove last word:", text.substring(0, text.lastIndexOf(" ")));
console.log("Replace char:", text.replaceAll(targetCharToReplace, replaceWithChar));








