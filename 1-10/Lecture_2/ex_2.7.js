
// EX 2.7


// Open Terminal and type "node ex_2.7.js" to run the file

/* TEHTÄVÄ
Create a program that loops through numbers from 1 to 100 and...

if the number is divisible by 3, prints “Fizz”
if the number is divisible by 5, prints “Buzz”
if the number is divisible by both (3 and 5), prints “FizzBuzz”
if no previous conditions apply, prints just the number
*/

for (let i = 0; i <= 100; i++) {
    const divisibleBy3 = i % 3 == 0;
    const divisibleBy5 = i % 5 == 0;

    if (divisibleBy3 && divisibleBy5)
        console.log("FizzBuzz");
    else if (divisibleBy3)
        console.log("Fizz");
    else if (divisibleBy5)
        console.log("Buzz");
    else
        console.log(i);
}
