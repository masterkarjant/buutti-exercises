
// EX 2.10


// Open Terminal and type "node ex_2.10.js" to run the file

/* TEHTÄVÄ

Find the highest, and the lowest scoring students.
Then find the average score of the students.
Print out only the students who scored higher than the average.
Assign grades (1-5) to all students based on their scores
    "1": "1-39",
    "2": "40-59",
    "3": "60-79",
    "4": "80-94",
    "5": "95-100"
*/

const students = [
    { name: "Markku", score: 99 },
    { name: "Karoliina", score: 58 },
    { name: "Susanna", score: 69 },
    { name: "Benjamin", score: 77 },
    { name: "Isak", score: 49 },
    { name: "Liisa", score: 89 },
];

let highestScoringStudent = students[0];
let lowestScoringStudent = students[0];
let sumOfScores = 0;
let numberOfStudents = students.length;

students.forEach(student => {
    if (highestScoringStudent.score < student.score)
        highestScoringStudent = student;
    if (lowestScoringStudent.score > student.score)
        lowestScoringStudent = student;

    if (student.score >= 95)
        student["grade"] = 5;
    else if (student.score >= 80)
        student["grade"] = 4;
    else if (student.score >= 60)
        student["grade"] = 3;
    else if (student.score >= 40)
        student["grade"] = 2;
    else if (student.score >= 1)
        student["grade"] = 1;

    sumOfScores = sumOfScores + student.score;
});

const averageScore = sumOfScores / numberOfStudents;

students.forEach(student => {
    if (student.score > averageScore)
        console.log("student with score above average", student.name);
});

console.log("average score", averageScore);
console.log("highest scoring student", highestScoringStudent);
console.log("lowest scoring student", lowestScoringStudent);

