
// EX 2.16


// Open Terminal and type "node ex_2.16.js" with a number between 1-12 eg. node ex_2.16.js 12

/* TEHTÄVÄ

Create a program that takes in a number from commandline that represents month of the year. 
Use console.log to show how many days there are in the given month number.
*/

const args = process.argv;
const monthNumber = parseInt(args[2]);

const monthsWith30Days = [4, 6, 9, 11];
const monthsWith31Days = [1, 3, 5, 7, 8, 10, 12];

if (monthNumber == 2)
    console.log("Month no.2 February is a special case. It has either 28 or 29 days, depending on the year");
else if (monthsWith30Days.includes(monthNumber))
    console.log("30 days in month number " + monthNumber);
else if (monthsWith31Days.includes(monthNumber))
    console.log("31 days in month number " + monthNumber);
else
    console.log("invalid input, must be a number between 1 and 12");
