
// EX 2.8


// Open Terminal and type "node ex_2.8.js" to run the file

/* TEHTÄVÄ
Create a program that takes in a number n and prints out a triangle of &’s with the height of n

Level 1: if n = 4, program prints
&
&&
&&&
&&&&

Level 2:							
    &
   &&&
  &&&&&
 &&&&&&&

 Level 3: 
Using the other loop compared to what you originally used, complete the same task.

E.g. if you solved this using for - loops, now solve it using while loops
*/

const n = 4;

console.log("Level 1 FOR");
let printRow = "";
for (let i = 0; i < n; i++) {
    printRow = printRow + "$";
    console.log(printRow);
}

console.log("Level 1 WHILE");
let i = 0;
printRow = "";
while (i < n) {
    printRow = printRow + "$";
    console.log(printRow);
    i++;
}

console.log("Level 2 FOR");
printRow = "";
for (let i = 0; i < n; i++) {
    let spaces = "";
    for (let i2 = i + 1; i2 < n; i2++) {
        spaces = spaces + " ";
    }
    if (i > 0)
        for (let i3 = 1; i3 <= 2; i3++) {
            printRow = printRow + "$";
        }
    else
        printRow = printRow + "$";

    console.log(spaces + printRow);
}

console.log("Level 2 WHILE");
i = 0;
printRow = "";
while (i < n) {
    let spaces = "";
    let i2 = i + 1;
    while (i2 < n) {
        i2++;
        spaces = spaces + " ";
    }
    if (i > 0) {
        let i3 = 1;
        while (i3 <= 2) {
            i3++;
            printRow = printRow + "$";
        }
    }
    else
        printRow = printRow + "$";

    console.log(spaces + printRow);
    i++;
}

