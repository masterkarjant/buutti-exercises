
// EX 2.5


// Open Terminal and type "node ex_2.5.js" to run the file

/* TEHTÄVÄ
Write a program that prints the sum of integers from 1 to n, with a given number n. For example, if n = 5, program prints 15.

Solve this task with both a for loop and a while loop.
*/

const n = 5;

let i = 0;
let sum = 0;
while (i <= n) {
    sum += i;
    i++;
}

console.log(sum);

sum = 0;
for (i = 0; i <= n; i++) {
    sum += i;
}

console.log(sum);