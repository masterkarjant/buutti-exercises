
// EX 2.14


// Open Terminal and type "node ex_2.14.js" to run the file, you can also add arguments eg. "node ex_2.14.js 1 2"

/* TEHTÄVÄ

Create a program that takes in two numbers a and b from the command line.
Print out "a is greater" if a is bigger than b, and vice versa, and "they are equal" if they are equal
Modify program to take in a third string argument c, and print out "yay, you guessed the password", if a and b are equal AND c is "hello world"

Remember to test that the program outputs the right answer in all cases.
*/

const args = process.argv;

const a = args[2];
const b = args[3];
const c = args[4];

if (a == b && c == "hello world")
    console.log("yay, you guessed the password");
else if (b > a)
    console.log("b is greater");
else if (a > b)
    console.log("a is greater");
else
    console.log("they are equal");