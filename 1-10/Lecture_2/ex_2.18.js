
// EX 2.18


// Open Terminal and type "node ex_2.18.js" with 3 names eg node ex_2.18.js a ab abc

/* TEHTÄVÄ

Create a program that takes in 3 names and outputs only initial letters of those name separated with dot.
example: node .\initialLetters.js Jack Jake Mike -> j.j.m
1.11 String length comparison
Create a program that takes in 3 names, and compares the length of those names. Print out the names ordered so that the longest name is first.
example: node .\lengthcomparison.js Maria Joe Philippa -> Philippa Maria Joe
*/

const args = process.argv;
const names = [args[2], args[3], args[4]];
let longest = names[0];
let middle = names[0];
let shortest = names[0];
const name1 = args[2];
const name2 = args[3];
const name3 = args[4];

console.log(`${name1[0]}.${name2[0]}.${name3[0]}`);

names.forEach(name => {
    if (name.length > longest.length)
        longest = name;
    if (name.length < shortest.length)
        shortest = name;
});

names.forEach(name => {
    if (name != longest && name != shortest)
        middle = name;
});

console.log(longest, middle, shortest);






