
// EX 2.17


// Open Terminal and type "node ex_2.17.js"

/* TEHTÄVÄ

Create a ATM program to check your balance. Create variables balance, isActive, checkBalance. Write conditional statement that implements the flowchart below.

Change the values of balance, checkBalance, and isActive to test your code!
*/



const balance = 200;
const isActive = true;
const checkBalance = true;

if (!checkBalance)
    console.log("Have a nice day");
else {

    if (isActive && balance > 0)
        console.log("Balance", balance);
    else if (!isActive)
        console.log("Your account is not active");
    else if (balance == 0)
        console.log("Your account is empty");
    else
        console.log("Your balance is negative");
}


