
// EX 2.4


// Open Terminal and type "node ex_2.4.js" to run the file

/* TEHTÄVÄ
Using for loop for each problem, print out the following number sequences:

0 100 200 300 400 500 600 700 800 900 1000
1 2 4 8 16 32 64 128
3 6 9 12 15
9 8 7 6 5 4 3 2 1 0
1 1 1 2 2 2 3 3 3 4 4 4
0 1 2 3 4 0 1 2 3 4 0 1 2 3 4
*/

console.log("t1");
for (let num = 0; num <= 1000; num += 100) {
    console.log(num);
}

console.log("t2");
for (let num = 1; num <= 128; num *= 2) {
    console.log(num);
}

console.log("t3");
for (let num = 3; num <= 15; num += 3) {
    console.log(num);
}

console.log("t4");
for (let num = 9; num >= 0; num -= 1) {
    console.log(num);
}

console.log("t5");
for (let num = 1; num < 5; num++) {
    console.log(num);
    console.log(num);
    console.log(num);
}

console.log("t6");
for (let loop = 0; loop < 3; loop++) {
    for (let num = 0; num <= 4; num++) {
        console.log(num);
    }
}