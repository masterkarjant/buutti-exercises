
// EX 2.15


// Open Terminal and type "node ex_2.15.js" with three numbers eg. node ex_2.15.js 1 2 3

/* TEHTÄVÄ

From the command line read in three numbers, number1, number2 and number3. Decide their values freely.
Find the
a) largest one
b) smallest one
c) if they all are equal, print that out
console.log() its name and value.
*/

const args = process.argv;

const numbers = [];
numbers.push(parseInt(args[2]), parseInt(args[3]), parseInt(args[4]));

let largest = numbers[0];
let smallest = numbers[0];

numbers.forEach(number => {
    if (number > largest)
        largest = number;
    if (number < smallest)
        smallest = number;
});

console.log("largest", largest);
console.log("smallest", smallest);

if (numbers[0] == numbers[1] && numbers[0] == numbers[2])
    console.log("numbers are equal");
