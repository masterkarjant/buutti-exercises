
// EX 2.3


// Open Terminal and type "node ex_2.3.js" to run the file

/* TEHTÄVÄ
Print the 3rd and 5th items of the array and the array’s length
Then sort the array in alphabetical order and print the entire array
Finally, add the item “sipuli” to the array, and print out again

EXTRA:
Remove the first item in the array, and print out again. HINT: shift()
Print out every item in this array using .forEach()
Print out every item that contains the letter ‘r’. HINT: includes()
*/

const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];

console.log(1, arr[2]);
console.log(2, arr[4]);

console.log(3, arr.sort());

arr.push("sipuli");
console.log(4, arr);

arr.shift(1);
console.log(5, arr);

console.log(6);
arr.forEach(x => console.log(x));

console.log(7);
arr.forEach(x => {
    if (x.includes("r"))
        console.log(x);
});
