
// EX 2.6


// Open Terminal and type "node ex_2.6.js" to run the file

/* TEHTÄVÄ
Modify the previous program so that only the multiples of three and five are considered in the sum. For example: if n=17, we’d accept 3, 5, 6, 9, 10, 12 and 15, and the sum would be 60.
*/

const n = 17;

let i = 0;
let sum = 0;
while (i <= n) {
    if (i % 3 == 0 || i % 5 == 0)
        sum += i;

    i++;
}

console.log(sum);

sum = 0;
for (i = 0; i <= n; i++) {
    if (i % 3 == 0 || i % 5 == 0)
        sum += i;
}

console.log(sum);