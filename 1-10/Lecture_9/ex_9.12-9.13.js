
import express from "express";
import helmet from "helmet";
const server = express();

// Create an express REST-API, which offers CRUD for your books. It should include following endpoinst


// GET /api/v1/books → Returns a list of all the books

// GET /api/v1/books/{id} → Returns a book with a corresponding ID.

// POST /api/v1/books → Creates a new book.

// PUT /api/v1/books/{id} → Modifies an existing book

// DELETE /api/v1/books/{id} → Removes a book with a corresponding id

// Every book should have four parameters

// id (number)
// name (string)
// author (string)
// read (boolean)

// MIDDLEWARE
const unknownEndpoint = (_req, res, _next) => {
    res.status(404).send({ error: "Error 404, Unknown Endpoint" });
};

const logger = (req, _res, next) => {
    const date = new Date();
    console.log("req made at:", date.toLocaleString()); // Tämän voi tosiaan esittää new Date().toLocaleString()
    console.log("req method:", req.method);
    console.log("req url endpoint:", req.url);
    if (Object.keys(req.body).length > 0)
        console.log("req body:", req.body);

    if (Object.keys(req.params).length > 0)
        console.log("req parameters:", req.params);

    if (Object.keys(req.query).length > 0)
        console.log("req query:", req.query);

    next();
};

const checkIfAllRequiredBodyDataIsPresentForBookPost = (req, res, next) => {
    if (req.body.id && req.body.name && req.body.author && req.body.read) {
        next();
    }
    else {
        res.status(400).send();
        // 404 on virhekoodi sille, että sivua ei löydy. Tässä tapauksessa
        // virhe on, että parametreja puuttuu, joten tulee käyttää virhekoodia
        // 400 Bad Request
    }
};


// USE
server.use(express.json());
server.use(helmet());
// server.use(logger);

// VAR
const books = [];

// GET

server.get("/", (_req, res) => {
    res.send(`
    <h1>BOOKS API</h1>
    `);
});

server.get("/api/v1/books", (req, res) => {
    res.send(books);
});

server.get("/api/v1/books/:id", (req, res) => {
    console.log("here", req.params.id);
    const bookIndex = books.findIndex(x => x.id === req.params.id);
    // typeof req.params.id === 'string', mutta typeof books[i].id === 'number', joten tämä ei toimi
    console.log(typeof req.params.id, typeof books[0].id);
    if (bookIndex > -1)
        res.send(books[bookIndex]);
    else {
        res.status(404).send();
    }
});

// POST
server.post("/api/v1/books", checkIfAllRequiredBodyDataIsPresentForBookPost, (req, res) => {
    books.push({ id: req.body.id, name: req.body.name, author: req.body.author, read: req.body.read });
    res.status(201).send(books);
});

// // PUT
server.put("/api/v1/books/:id", (req, res) => {
    const bookIndex = books.findIndex(x => x.id === req.params.id);
    // typeof req.params.id === 'string', mutta typeof books[i].id === 'number', joten tämä ei toimi
    if (bookIndex > -1) {
        books[bookIndex] = { id: req.body.id || books[bookIndex].id, name: req.body.name || books[bookIndex].name, author: req.body.author || books[bookIndex].author, read: req.body.read || books[bookIndex].read };
        res.status(204).send();
    }
    else
        res.status(404).send();
});

// // DELETE
server.delete("/api/v1/books/:id", (req, res) => {
    const bookIndex = books.findIndex(x => x.id === req.params.id);
    if (bookIndex > -1) {
        books.splice(bookIndex, 1);
        res.send("Book removed from the books list");
    }
    else {
        res.status(404).send();
    }
});

server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});

/*
PUT, DELETE ja GET /:id endpointit palauttavat kaikki 404.
Tätä koodia ei selvästi ole testattu postmanilla ollenkaan.
*/