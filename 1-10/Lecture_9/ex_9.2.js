import express from "express";

const server = express();

server.listen(3000, () => {
    console.log("Listening to port 3000");
});
server.get("/", (request, response) => {
    response.send("Hello world!");
});
server.get("/other", (request, response) => {
    response.send("World hello!");
});

