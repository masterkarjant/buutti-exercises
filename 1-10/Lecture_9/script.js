


const BASE_URL = "http://localhost:3000/api/v1/books";

let booksListArr = [];
// eslint-disable-next-line no-unused-vars
async function apiFetch() {
    try {
        const response = await fetch(BASE_URL);
        const jsonData = await response.json();
        return {
            booksListArr: jsonData.data,
        };
    }
    catch (error) {
        console.log(error);
    }
}

apiFetch();


// async function fetchRestaurantDataAndRender() {
//     // apiFetch() from apiFetch.js
//     // eslint-disable-next-line no-undef
//     const data = await apiFetch()
//     restaurantData = data.restaurants
//     tagData = data.tags
//     render(restaurantData)
// }