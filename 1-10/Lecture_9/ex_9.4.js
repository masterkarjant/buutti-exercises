import express from "express";

const server = express();

let counter = 0;
const counterObject = {};

server.get("/", (request, response) => {
    counter++;
    response.send(`<h1>${counter}</h1>`);
});

server.get("/counter/:name", (request, response) => {
    console.log("here", request.query);

    let add = 1;
    if (request.query.add)
        add = parseInt(request.query.add);

    if (counterObject[request.params.name])
        counterObject[request.params.name] = counterObject[request.params.name] + add;
    else
        counterObject[request.params.name] = add;

    let html = `<h1>${request.params.name} was here ${counterObject[request.params.name]} times</h1>`;
    response.send(html);
});

server.get("/counter", (request, response) => {
    console.log(request.query);

    let html = "";
    Object.entries(request.query).forEach(entry => {
        if (counterObject[entry[0]])
            counterObject[entry[0]] = parseInt(entry[1]) + counterObject[entry[0]];
        else
            counterObject[entry[0]] = parseInt(entry[1]);

        console.log(counterObject[entry[0]]);
        html = html + `<h1>${entry[0]} was here ${counterObject[entry[0]]} times</h1>`;
    });

    response.send(html);
});


server.listen(3000, () => {
    console.log("Listening to port 3000");
});