import http from "http";

const server = http.createServer((_req, res) => {
    res.write("Hello world!");
    res.end();
});
server.listen(5000);

console.log("Listening port 5000");