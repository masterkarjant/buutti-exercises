import express from "express";

const server = express();

let counter = 0;

server.get("/", (request, response) => {
    counter++;
    response.send(`<h1>${counter}</h1>`);
});
server.get("/other", (request, response) => {
    response.send("World hello!");
});
server.listen(3000, () => {
    console.log("Listening to port 3000");
});