
import express from "express";

const server = express();


// MIDDLEWARE
const numberCheck = (req, res, next) => {
    console.log("query", req.query);
    console.log("param", req.params);

    if (req.query.numbers && req.query.numbers.length)
        res.locals.numbersArr = turnStringToNumbers(req.query.numbers);
    // Tästä muuten syntyy helposti virheitä, kun käyttää useamman rivin if-lauseita
    // ilman koodiblokkia (eli kaarisulkuja)

    console.log(res.locals.numbersArr);

    if (numbersArr.includes("error") || !req.query.numbers)
        return res.status(400).send("<h1>error: invalid query</h1>");

    next();

    // Sen sijaan, että käytät middlewaressa koko tiedoston scopessa olevaa muuttujaa, 
    // tallenna tulos req parametriin, jolloin se on sieltä käytettävissä kaikissa 
    // tulevissa middlewareissa. Tämä on myös tarpeen kun haluat siirtää middlewaret
    // omaan tiedostoonsa.
};

const unknownEndpoint = (_req, res, _next) => {
    res.status(404).send({ error: "Error 404, Unknown Endpoint" });
};

// VAR
let numbersArr = [];

// GET BASE
server.get("/", (_req, res) => {
    res.send(`
        <h4> use GET url endpoints for /divide /multiply /add /substract</h4>
        <h4> use ?numbers= query with commas separating the numbers</h4>
    `);
});

// USE
server.use(express.json());
server.use(numberCheck);

// GET
server.get("/divide", (_req, res) => {
    let quotient = 0;
    let response = `<h1>Answer: ${quotient}</h1>`;
    // Tämä ei toimi. Nyt response määritellään nollaksi ja sitä ei muuteta 
    // koodissa missään kohdassa paitsi nollalla jako -virheessä.
    // Muuttuja response pitää määritellä vasta kun quotient on laskettu.

    res.locals.numbersArr.forEach((num, i) => {
        if (i == 0)
            response = `<h1>Answer: ${quotient = num}</h1 >`;
        else if (num !== 0)
            response = `<h1>Answer: ${quotient / num}</h1 >`;
        else
            response = "<h1>You cannot divide by 0</h1>";
    });

    res.send(response);
});

server.get("/multiply", (_req, res) => {
    let product = 0;

    res.locals.numbersArr.forEach((num, i) => {
        if (i == 0)
            product = num;
        else
            product = product * num;
    });

    res.send(`
    < h1 > Answer: ${product}</h1 >
    `);
});

server.get("/substract", (_req, res) => {
    let difference = 0;

    res.locals.numbersArr.forEach((num, i) => {
        if (i == 0)
            difference = num;
        else
            difference = difference - num;
    });

    res.send(`
    < h1 > Answer: ${difference}</h1 >
    `);
});

server.get("/add", (_req, res) => {
    const sum = res.locals.numbersArr.reduce((acc, curr) => acc + curr, 0);
    res.send(`
    < h1 > Answer: ${sum}</h1 >
    `);
});

server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});

// MISC

function turnStringToNumbers(numbersString) {
    const numbersArr = numbersString.split(",").map(number => {
        const harvestValue = parseInt(number);
        if (!isNaN(harvestValue))
            return harvestValue;
        else
            return "error";
    });

    return numbersArr;
}

/*
Kaikenkaikkiaan hyvän näköistä koodia. Tässä näkyy jo selkeästi, kuinka seuraavalla
luennolla esitelty Router selkeyttäisi koodia. Nyt samassa tiedostossa
on serverimäärittelyä, reittien määrittelyä, ja funktioita.

Middlewaret ja apufunktiot olisi ehkä voinut olla eri tiedostossa, mutta 
koodia kuitenkin on reilu sata riviä, joten sekin on vähän siinä ja siinä.
Ne olisi kuitenkin tässä tapauksessa kannattanut sijoittaa tiedostoon hieman
organisoidummin.

Lähtökohtaisesti yleensä halutaan, että API, kuten funktiotkin, palauttavat
ainoastaan tuloksen ilman muotoilua. Eli tulevaisuudessa jätä header-tagit
pois ja palauta ainoastaan tulos joko numerona tai JSON-objektina. Sitten
käyttäjä voi muotoilla sen haluamallaan tavalla.
*/