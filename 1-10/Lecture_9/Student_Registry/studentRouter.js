
import express from "express";

const router = express.Router();

// VAR
const students = {};

// GET
router.get("/students", (_req, res) => {
    res.send(students);
});

router.get("/student/:id", (req, res) => {
    if (students[req.params.id])
        res.send(students[req.params.id]);
    else {
        res.status(404).send();
    }
});

// POST
router.post("/student", (req, res) => {
    if (req.body.id && req.body.name && req.body.email) {
        students[req.body.id] = req.body;
        res.status(201).send();
    }
    else {
        res.status(404).send();
    }
});

// PUT
router.put("/student/:id", (req, res) => {
    if (!students[req.params.id])
        res.status(404).send();

    if (req.body.name && req.body.email) {
        students[req.body.id] = { ...req.body };
        res.status(204).send();
    }
    else {
        res.status(400).send("error: missing name or email");
    }
});

// DELETE
router.delete("/student/:id", (req, res) => {
    if (!students[req.params.id])
        res.status(404).send();
    delete students[req.params.id];
    res.send("student removed from the student list");
});

export default router;