
import express from "express";
import studentRounter from "./studentRouter.js";

const server = express();

// MIDDLEWARE
const logger = (req, _res, next) => {
    const date = new Date();
    console.log("req made at:", date.toLocaleString());
    console.log("req method:", req.method);
    console.log("req url endpoint:", req.url);
    if (Object.keys(req.body).length > 0)
        console.log("req body:", req.body);

    next();
};
const unknownEndpoint = (_req, res, _next) => {
    res.status(404).send({ error: "Error 404, Unknown Endpoint" });
};

// USE
server.use(express.json());
server.use(express.static("public"));
server.use(logger);

server.use("/", studentRounter);

server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});
// server.get("/counter/:name", (req, res) => {
//     console.log("here", req.query);

//     let add = 1;
//     if (req.query.add)
//         add = parseInt(req.query.add);

//     if (counterObject[req.params.name])
//         counterObject[req.params.name] = counterObject[req.params.name] + add;
//     else
//         counterObject[req.params.name] = add;

//     let html = `<h1>${req.params.name} was here ${counterObject[req.params.name]} times</h1>`;
//     res.send(html);
// });

// server.get("/counter", (req, res) => {
//     console.log(req.query);

//     let html = "";
//     Object.entries(req.query).forEach(entry => {
//         if (counterObject[entry[0]])
//             counterObject[entry[0]] = parseInt(entry[1]) + counterObject[entry[0]];
//         else
//             counterObject[entry[0]] = parseInt(entry[1]);

//         console.log(counterObject[entry[0]]);
//         html = html + `<h1>${entry[0]} was here ${counterObject[entry[0]]} times</h1>`;
//     });

//     res.send(html);
// });

