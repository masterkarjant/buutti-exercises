// Tämä antaa nyt aika erilaisia tuloksia kuin omani, esimerkiksi Moby Dick word count on tämän mukaan vain 37639.
// Lisäksi prosessointiin menee sen verran kauan, että oletin ohjelman jo kaatuneen.

const wordsArr = [];
function analyze() {
    const results = document.getElementById("results");

    if (document.getElementById("container"))
        document.getElementById("container").remove();

    const container = document.createElement("div");
    container.id = "container";
    results.appendChild(container);

    if (document.getElementById("area").value.length > 10000) {
        const loading = document.createElement("h3");
        loading.innerHTML = "LOADING, over 10000 words detected, " + document.getElementById("area").value.length + " to be exact. Will take a bit to process...";
        loading.id = "loading";
        container.appendChild(loading);
    }

    setTimeout(() => {
        const pTotalWords = document.createElement("p");
        const pAverageLength = document.createElement("p");
        const wordCountList = document.createElement("ul");

        const wordCount = getWordCountArray(); // <- has to be above averageLength
        const averageLength = getAverageWordLength();
        const longestWord = getLongestWord(wordCount); // En ymmärrä mihin tarvitaan pisintä sanaa
        const mostFrequentlyUsedWordsAndCountArr = getSortedWordCountArray(longestWord, wordCount);

        pTotalWords.innerHTML = `Total words in text: ${wordsArr.length}`;
        pAverageLength.innerHTML = `Average word length: ${averageLength}`;

        const countHeader = document.createElement("h4");
        countHeader.innerHTML = "How many times a word repeats in the text";
        wordCountList.appendChild(countHeader);


        mostFrequentlyUsedWordsAndCountArr.forEach(word => {
            const wordListItem = document.createElement("li");
            wordListItem.innerHTML = `${Object.keys(word)[0]} : ${Object.values(word)[0]}`;
            wordCountList.appendChild(wordListItem);
        });

        if (document.getElementById("loading"))
            document.getElementById("loading").remove();

        container.appendChild(pTotalWords);
        container.appendChild(pAverageLength);
        container.appendChild(wordCountList);
    }, 10);
    // En ymmärrä miksi tässä on setTimeout
}

function getAverageWordLength() {
    const lengthArr = wordsArr.map(x => x.length);
    // Huomaa, että wordsArr pitää sisällään ainoastaan uniikit sanat. 
    // Jos esim teksti on "pylly vasten pyllyä pum pum", niin siellä on "pum" vain yhden kerran.
    // Sanan keskipituuden pitäisi olla (5 + 6 + 5 + 3 + 3) / 5, mutta siihen tuleekin vain (5 + 6 + 5 + 3) / 4
    let lengthSum = 0;
    lengthArr.forEach(x => {
        lengthSum = lengthSum + x;
    });
    return lengthSum / lengthArr.length;
}

function getWordCountArray() {
    const targetText = document.getElementById("area").value;
    const targetTextSimplified = targetText.replaceAll(",", "").replaceAll(".", "").toLowerCase();

    const wordCount = {};
    targetTextSimplified.split(" ").forEach(word => {
        if (!wordsArr.includes(word))
            wordsArr.push(word);
        // Tämä vaikuttaa ohjelman nopeuteen tosi paljon. Nyt jokaisen sanan kohdalla .includes joutuu käymään läpi koko wordsArr listan.
        // Kun listassa alkaa olla paljon sanoja, tämä kasvattaa prosessorivaatimusta eksponentiaalisesti

        // Lisäksi tämä funktio nyt muuttaa "sivuvaikutuksena" funktion ulkopuolista muuttujaa. Funktion nimi ei anna viitettä
        // siihen, että se näin tekisi, eikä lukijalle ole selvää, miksi näin tapahtuu.
        wordCount[word] = wordCount[word] + 1 || 1;
    });

    return wordCount;
}

function getSortedWordCountArray(longestWord, wordCount) {
    const mostFrequentlyUsedWordsAndCountArr = [];
    for (let index = longestWord; index != 0; index--) {

        Object.entries(wordCount).forEach(x => {
            if (x[1] == index) {
                const word = x[0];
                mostFrequentlyUsedWordsAndCountArr.push({ [word]: x[1] });
            }
        });
    }
    return mostFrequentlyUsedWordsAndCountArr;
}

function getLongestWord(wordCount) {
    let longestWord = 0;
    Object.entries(wordCount).forEach(x => {
        if (longestWord < x[1]) longestWord = x[1];
    });
    return longestWord;
}

// Ratkaisu toimii, ja pystyy käsittelemään pitkänkin tekstin.
// Sen ongelmat ovat epäoptimaalisuus (josta ei olla puhuttu ollenkaan, joten siitä ei kannata ottaa stressiä)
// ja kompleksisuus. Jälkimmäisen vähentämiseksi kannattaa jatkossa panostaa, jotta koodista
// tulee helposti luettavaa.