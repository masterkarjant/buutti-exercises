

// Write a script that

// Divides each sentence into a separate paragraph.
// Highlights all words longer than 6 letters in yellow color.
// Removes the original paragraph(so that there is no duplicate text)

const pContainer = document.getElementById("pContainer");
const firstP = pContainer.querySelector("p").innerHTML;
pContainer.querySelector("p").remove();

const sentenceArr = firstP.split(".");

sentenceArr.forEach(x => { // käytät alla hyvää tapaa nimetä arrayn (words) ja sen elementit (word), samaa olisi voinut käyttää tässä
    const paragraph = document.createElement("p");
    let sentence = "";
    const words = x.split(" ");
    words.forEach((word, i) => {
        if (word.length > 6) {
            word = `<span style="color:yellow;">${word}</span>`;
        }
        sentence = sentence + word;
        if (i < words.length - 1)
            sentence = sentence + " ";
    });
    // Tässä olisi ehkä ollut sekeämpää käyttää const sentence = x.split(...).map(...).join(...)
    paragraph.innerHTML = sentence;
    pContainer.appendChild(paragraph);
});
