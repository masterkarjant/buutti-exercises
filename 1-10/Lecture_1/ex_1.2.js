
// EX 1.2


// Open Terminal and type "node ex_1.2.js" to run the file, you can also add arguments eg. "node ex_1.2.js 5 11"

const arg = process.argv;

const a = arg[2] || 9;
const b = arg[3] || 2;

const sum = a + b;
const difference = a - b;
const fraction = a / b;
const product = a * b;

console.log("sum", sum);
console.log("difference", difference);
console.log("fraction", fraction);
console.log("product", product);

const exponentiation = a ** b;
const modulo = a % b;

console.log("exponentiation", exponentiation);
console.log("modulo", modulo);