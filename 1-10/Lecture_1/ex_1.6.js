
// EX 1.6


// Open Terminal and type "node ex_1.6.js" to run the file

// TEHTÄVÄ: Create variables for price and discount and assign some values for those. Calculate and console.log discounted price.

const price = 20;
const discountPercentage = 65;

console.log("discounted price", price * (discountPercentage / 100));