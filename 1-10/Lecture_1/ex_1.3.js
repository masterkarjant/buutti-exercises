
// EX 1.3


// Open Terminal and type "node ex_1.3.js" to run the file

const str1 = "Anton";
const str2 = "Karjakin";

const str_sum = str1 + str2;
const str_average = str_sum.length / 2;

console.log("str_sum", str_sum);
console.log("str1_length", str1.length);
console.log("str2_length", str2.length);
console.log("str_average", str_average);

if (str1.length < str_average)
    console.log(str1);

if (str2.length < str_average)
    console.log(str1);

if (str_sum.length < str_average)
    console.log(str1);