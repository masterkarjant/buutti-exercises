
// EX 1.9


// Open Terminal and type "node ex_1.9.js" + a number to run the file, eg "node ex_1.9.js 50"

/* 
TEHTÄVÄ: Create a program that takes in a number from command line that represents a length of a squares sides. Calculate the area of the square with given number.
A square with sides of length 5m is 25 square meters in area.
*/

const arg = process.argv;

const squareSideLengthInMeters = arg[2];
const squareMeters = squareSideLengthInMeters * squareSideLengthInMeters;

if (!squareSideLengthInMeters)
    console.log("Input a number after .js to calculate it's m2");
else
    console.log("square meters", squareMeters);
