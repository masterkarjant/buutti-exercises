
// EX 1.4


// Open Terminal and type "node ex_1.4.js" to run the file

// a
const playerCount = 4;

if (playerCount >= 4)
    console.log("a: We're playing hearts with " + playerCount + " people");
else
    console.log("a: We're not playing hearts as we are only " + playerCount + " people");

// b
const isStressed = true;
const hasIcecream = true;

if (!isStressed || hasIcecream)
    console.log("b: Mark is happy");
else
    console.log("b: Mark is not happy");

// c
const sunIsShining = true;
const isNotRaining = true;
const temperatureInCelsius = 35;

if (sunIsShining && isNotRaining && temperatureInCelsius >= 20)
    console.log("c: It's a beach day");
else
    console.log("c: Not a beach day, but it's a good weather for indoors");

// d
const seeSuzy = true;
const seeDan = false;

if (!(seeSuzy && seeDan) && !(!seeSuzy && !seeDan))
    console.log("d: Arin is happy");
else
    console.log("c: Arin is not happy");