
// EX 1.7


// Open Terminal and type "node ex_1.7.js" to run the file

// TEHTÄVÄ: Create variables for distance (kilometers) and speed (km / h). Calculate and console.log travel time.

const distance = 65;
const speed = 120;

console.log("travel time in minutes", Math.floor((distance / speed) * 60));