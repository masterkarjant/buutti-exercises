
// EX 1.5


// Open Terminal and type "node ex_1.5.js" to run the file, you can also add arguments eg. "node ex_1.5.js 'Random message'"

const arg = process.argv;

const inputString = arg[2] || "Solve the puzzle, add a correct string after .js as you run the script ";

if (inputString != inputString.trim())
    displayRules("Issue with Rule 1");
else if (inputString.length > 20)
    displayRules("Issue with Rule 2");
else if (inputString[0] == inputString[0].toUpperCase())
    displayRules("Issue with Rule 3");
else
    console.log(inputString);

function displayRules(errorMsg) {
    console.error("Issue with displaying the message, check that the rules are respected");
    console.log("Rule 1: Message has no white spaces in the beginning or end");
    console.log("Rule 2: Message has a maximum length of 20");
    console.log("Rule 2: Message never starts with a capital letter");

    console.log("ERROR:", errorMsg);
}

