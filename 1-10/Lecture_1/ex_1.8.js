
// EX 1.8


// Open Terminal and type "node ex_1.8.js" to run the file


// TEHTÄVÄ Calculate how many seconds there are in a year. Use variables for days, hours, seconds and seconds in a year. Print out the result with console.log.

const daysInYear = 365;
const hoursInYear = daysInYear * 24;
const minutesInYear = hoursInYear * 60;
const secondsInYear = minutesInYear * 60;

// console.log("days in a year", daysInYear)
// console.log("hours in a year", hoursInYear)
// console.log("minutes in a year", minutesInYear)
console.log("seconds in a year", secondsInYear);