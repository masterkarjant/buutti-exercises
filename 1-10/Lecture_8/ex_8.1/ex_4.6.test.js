import { calculator } from "../ex_8.1/ex_4.6";


// Helppo 

describe("Turha test", () => {
    it(" 5 + 5 = 10", () => {
        expect(calculator("+", 5, 5)).toBe(10);
    });

    it("5 - 5 = 0", () => {
        expect(calculator("-", 5, 5)).toBe(0);
    });

    it("issue with operator", () => {
        expect(calculator("b", 5, 5)).toBe("Error: Can't do that!");
    });
});

// Jakolasku


describe("Jako", () => {
    it("5 jaettuna 5 on 1", () => {
        expect(calculator("/", 5, 5)).toBe(1);
    });

    it("3 jaettuna 3 on 1", () => {
        expect(calculator("/", 3, 1)).toBe(3);
    });

    it("0 ei voi jakaa 0", () => {
        expect(calculator("/", 0, 0)).toBe(NaN);
    });
});

// Kertolasku 

test("passable test", () => {
    expect(calculator("*", 5, 5)).toBe(25);
});

test("wrong input test", () => {
    expect(calculator("*", "hello", 5)).toBe(NaN);
});

test("wrong input test", () => {
    expect(calculator("*", 5, "hello")).toBe(NaN);
});

test("wrong input test", () => {
    expect(calculator("*", "hello", "hello")).toBe(NaN);
});


