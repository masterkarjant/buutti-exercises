import { converter } from "./script";

describe("Test Conversion", () => {
    it("6 cup to liter", () => {
        expect(converter(6, "cup", "liter")).toBe(1.42);
    });

    it("6 cup to deciliter", () => {
        expect(converter(6, "cup", "deciliter")).toBe(14.2);
    });

    it("6 cup to ounce", () => {
        expect(converter(6, "cup", "ounce")).toBe(48);
    });

    it("6 cup to cup", () => {
        expect(converter(6, "cup", "cup")).toBe("invalid input");
    });

    it("6 cup to pint", () => {
        expect(converter(6, "cup", "pint")).toBe(3);
    });

});

// Tässä testataan nyt vain "helppoja" tapauksia
// Muista sisällyttää testeihin myös vaikat skenaariot