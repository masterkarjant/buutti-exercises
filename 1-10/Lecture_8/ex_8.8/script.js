// Write a command line unit converter that converts between volume units. Your converter should accept at least units Deciliter, liter, ounce, cup, and pint.
// The program takes three parameters: amount, source unit, and the target unit. For example
// convert 6 Deciliter oz --> 20
// Write tests for your converter. Include at least five tests!

// Tämä ei taida toimia command linelta ollenkaan?


// MALLIVASTAUS +_+
// const oz = 30  // one ounce is 30 ml (about)

// const toMillilitersRatio = {
//     ml: 1,
//     dl: 10,
//     l: 1000,
//     oz: oz,
//     c: 8 * oz,
//     pt: 16 * oz,
//     qt: 32 * oz,
//     gl: 128 * oz,
// }

// export const converter = (amount, fromUnit, toUnit) => toMillilitersRatio[fromUnit] * amount / toMillilitersRatio[toUnit]

// const [amount, fromUnit, toUnit] = process.argv.slice(2)

// console.log(converter(process.argv[2], process.argv[3], process.argv[4]))

const amount = 6;
const sourceUnit = "cup";
const targetUnit = "liter";

console.log(converter(amount, sourceUnit, targetUnit));

export function converter(amount, sourceUnit, targetUnit) {
    let convertedAmount;
    if (sourceUnit == "deciliter" && targetUnit == "ounce")
        convertedAmount = convertDeciliterToOunce(amount);
    else if (sourceUnit == "deciliter" && targetUnit == "cup")
        convertedAmount = convertDeciliterToCup(amount);
    else if (sourceUnit == "deciliter" && targetUnit == "pint")
        convertedAmount = convertDeciliterToPint(amount);
    else if (sourceUnit == "deciliter" && targetUnit == "liter")
        convertedAmount = convertDeciliterToLiter(amount);

    // OUNCE
    else if (sourceUnit == "ounce" && targetUnit == "deciliter")
        convertedAmount = convertOunceToDeciliter(amount);
    else if (sourceUnit == "ounce" && targetUnit == "cup")
        convertedAmount = convertOunceToCup(amount);
    else if (sourceUnit == "ounce" && targetUnit == "pint")
        convertedAmount = convertOunceToPint(amount);
    else if (sourceUnit == "ounce" && targetUnit == "liter")
        convertedAmount = convertOunceToLiter(amount);


    // CUP
    else if (sourceUnit == "cup" && targetUnit == "deciliter")
        convertedAmount = convertCupToDeciliter(amount);
    else if (sourceUnit == "cup" && targetUnit == "ounce")
        convertedAmount = convertCupToOunce(amount);
    else if (sourceUnit == "cup" && targetUnit == "pint")
        convertedAmount = convertCupToPint(amount);
    else if (sourceUnit == "cup" && targetUnit == "liter")
        convertedAmount = convertCupToLiter(amount);


    // PINT
    else if (sourceUnit == "pint" && targetUnit == "deciliter")
        convertedAmount = convertPintToDeciliter(amount);
    else if (sourceUnit == "pint" && targetUnit == "cup")
        convertedAmount = convertPintToCup(amount);
    else if (sourceUnit == "pint" && targetUnit == "ounce")
        convertedAmount = convertPintToOunce(amount);
    else if (sourceUnit == "pint" && targetUnit == "liter")
        convertedAmount = convertPintToLiter(amount);

    // LITER
    else if (sourceUnit == "liter" && targetUnit == "deciliter")
        convertedAmount = convertLiterToDeciliter(amount);
    else if (sourceUnit == "liter" && targetUnit == "cup")
        convertedAmount = convertLiterToCup(amount);
    else if (sourceUnit == "liter" && targetUnit == "pint")
        convertedAmount = convertLiterToPint(amount);
    else if (sourceUnit == "liter" && targetUnit == "ounce")
        convertedAmount = convertLiterToOunce(amount);

    // Tällaisissa tapauksissa olisi mielestäni selkeämpää muotoilla siten, 
    // että tapaukset on ryhmitelty esim sourceUnit mukaan
    /*
    if (sourceUnit === "liter") {
        if (targetUnit === "deciliter") {
            ///
        } else if (targetUnit === "cup") {
            ///
        }
        /// etc
    }
    */


    else
        return "invalid input";

    return parseFloat(convertedAmount.toFixed(2));
}

// Kaikenkaikkiaan tosi paljon koodia. Lisäksi aika vaikeasti ylläpidettävää. 
// Mitä jos muunnosyksiköitä olisikin 20? tai 2000?
// Vertaa malliratkaisuun

// DL

function convertDeciliterToOunce(amount) {
    return amount * 3.3814;
}

function convertDeciliterToCup(amount) {
    return amount * 0.422675;
}

function convertDeciliterToPint(amount) {
    return amount * 0.211338;
}

function convertDeciliterToLiter(amount) {
    return amount * 0.1;
}

// OUNCE

function convertOunceToDeciliter(amount) {
    return amount * 0.295735;
}

function convertOunceToCup(amount) {
    return amount * 0.125;
}

function convertOunceToPint(amount) {
    return amount * 0.0625;
}

function convertOunceToLiter(amount) {
    return amount * 0.0295735;
}



function convertCupToDeciliter(amount) {
    return amount * 2.36588;
}

function convertCupToOunce(amount) {
    return amount * 8;
}

function convertCupToPint(amount) {
    return amount * 0.5;
}

function convertCupToLiter(amount) {
    return amount * 0.236588;
}



function convertPintToDeciliter(amount) {
    return amount * 4.73176;
}

function convertPintToCup(amount) {
    return amount * 2;
}

function convertPintToOunce(amount) {
    return amount * 16;
}

function convertPintToLiter(amount) {
    return amount * 0.473176;
}



function convertLiterToDeciliter(amount) {
    return amount * 10;
}

function convertLiterToCup(amount) {
    return amount * 4.22675;
}

function convertLiterToPint(amount) {
    return amount * 2.11338;
}

function convertLiterToOunce(amount) {
    return amount * 33.814;
}

