
// EX 3.10


// Open Terminal and type "node ex_3.10.js" with a number between 0.1 - 360 eg. node ex_3.10.js 144

/* TEHTÄVÄ

Suppose airport runways are given numbers determined by the direction in which planes travel as they move along the runways. The number is found by taking the bearing in degrees and rounding to the nearest 10 degrees and then dropping the last 0. For example, if the bearing along the runway would be 267.5 degrees, it would be first rounded to 270 degrees and then discarded the last zero to get 27.

Write a program where the user tells the bearing (0-360deg) and the program outputs the corresponding runway number. Yes, this airport has exactly 36 runways going to all possible directions. Why else would they have a weird system like this?

Restriction: Don’t use round(), floor() or ceil() functions here!
Hint: The modulo operator might help instead.
*/

const args = process.argv;
let degrees = parseInt(args[2]);

let runwayNumber = 0;
let remainingDegrees = parseInt(args[2]);

for (let index = 0; index <= (degrees / 10); index++) {
    if (remainingDegrees > 10 || (remainingDegrees < 10 && remainingDegrees >= 5)) {
        runwayNumber++;
        remainingDegrees = remainingDegrees - 10;
    }
}

console.log(runwayNumber);