
// EX 3.16


// Open Terminal and type "node ex_3.16.js" with a word, eg node ex_3.16.js saippuakäpykauppias

/* TEHTÄVÄ

Check if given string is a palindrome.
Examples:
 node .\palindrome.js saippuakivikauppias -> Yes, 'saippuakivikauppias' is a palindrome
 node .\palindrome.js saippuakäpykauppias -> No, 'saippuakäpykauppias' is not a palindrome
*/

const args = process.argv;
let word = args[2];
let reversedWord = "";

word.split("").forEach((char, index) => {
    reversedWord = reversedWord + word[word.length - 1 - index];
});

if (word === reversedWord)
    console.log("Yes, '" + word + "' is a palindrome");
else
    console.log("No, '" + word + "' is not a palindrome");