
// EX 3.15


// Open Terminal and type "node ex_3.15.js" with a sentence. node ex_3.15.js "Bobo like's cheese"

/* TEHTÄVÄ

Create a programs that reverses each word in a string.
node .\reversed_words.js "this is a very long sentence" -> sihT si a yrev gnol ecnetnes

*/

const args = process.argv;
let sentence = args[2];

const reversedSentence = sentence.split(" ").map(word => {
    let reversedWord = "";
    word.split("").forEach((char, index) => {
        reversedWord = reversedWord + word[word.length - 1 - index];
    });
    return reversedWord;
}).join(" ");

console.log(reversedSentence);