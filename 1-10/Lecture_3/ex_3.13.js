
// EX 3.13


// Open Terminal and type "node ex_3.13.js" 

/* TEHTÄVÄ

Create program that outputs competitors placements with following way:
["1st competitor was Julia", "2nd competitor was Mark", "3rd competitor was Spencer", "4th competitor was Ann", "5th competitor was John", "6th competitor was Joe"]
*/

const competitors = ["Julia", "Mark", "Spencer", "Ann", "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

competitors.forEach((competitor, i) => {
    if (i < 4)
        console.log("" + (i + 1) + ordinals[i] + " competitor was " + competitor);
    else
        console.log("" + (i + 1) + ordinals[3] + " competitor was " + competitor);

});

