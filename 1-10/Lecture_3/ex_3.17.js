
// EX 3.17


// Open Terminal and type "node ex_3.17.js" with a number between 1-12 eg. node ex_3.17.js 12

/* TEHTÄVÄ

Create a program that every time you run it, prints out an array with differently randomized order of the array above.
Example:
node .\array_randomizer.js -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10]
*/

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32, 32];

let randomizedArray = array
    .map(value => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value)


// VANHA 
// while (randomizedArray.length != array.length) {
//     const randomIndex = Math.floor(Math.random() * array.length);
//     console.log(randomIndex);
//     if (!randomizedArray.includes(array[randomIndex]))
//         randomizedArray.push(array[randomIndex]);
// }

console.log(randomizedArray);

// Tämä ohjelma tosiaan tekee sen, mitä tehtävänannossa pyydetään, mutta
// se näyttää jumittuvan, mikäli alkuperäisessä syötteessä on kaksi samaa
// elementtiä.

// KORJATTU!