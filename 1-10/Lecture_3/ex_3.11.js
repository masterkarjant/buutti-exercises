
// EX 3.11


// Open Terminal and type "node ex_3.11.js" with an additional number eg. node ex_3.11.js 22

/* TEHTÄVÄ

Create a program that takes in a number from the command line, for example node .\countSheep.js 3 and prints a string "1 sheep...2 sheep...3 sheep..."
*/

const args = process.argv;
let howManySheeps = parseInt(args[2]);
let sheepString = "";

for (let index = 1; index <= howManySheeps; index++) {
    sheepString = sheepString + "" + index + " sheep...";
    // Jos indeksoinnin laittaa alkamaan ykkösestä, ei tarvitse laskutoimitusta koodissa
    // Tuo "" on tarpeeton, koska sheepString on tyyppiä string

    // KORJATTU
}

console.log(sheepString);