
// EX 3.12


// Open Terminal and type "node ex_3.12.js" with a word eg. node ex_3.12.js banana

/* TEHTÄVÄ

Create a program that turns any given word into charIndex version of the word
Example:
node .\charIndex.js "bead" ->  2514
node .\charIndex.js "rose" ->  1815195
*/

const args = process.argv;
let word = args[2];

const charIndex = { q: 1, w: 2, e: 3, r: 4, t: 5, y: 6, u: 7, i: 8, o: 9, p: 10, å: 0, a: 11, s: 12, d: 13, f: 14, g: 15, h: 16, j: 17, k: 18, l: 19, ö: 20, ä: 0, z: 21, x: 22, c: 23, v: 24, b: 25, n: 26, m: 27, };
// Indeksointi ei ole sama kuin tehtävänannossa, joten tulokset eivät vastaa esimerkkiä, mutta logiikka on toki oikea

let charIndexVersionOfWord = "";

word.split("").forEach(char => {
    charIndexVersionOfWord = charIndexVersionOfWord + charIndex[char];
});

console.log(charIndexVersionOfWord);