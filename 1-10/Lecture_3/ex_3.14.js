
// EX 3.14


// Open Terminal and type "node ex_3.14.js" with two additional numbers eg. node ex_3.14.js 12 25

/* TEHTÄVÄ

Write a program that takes in any two numbers from the command line, start and end. The program creates and prints an array filled with numbers from start to end.
Examples:
node .\createRange.js 1 5 -> [1, 2, 3, 4, 5]
node .\createRange.js -5 -1 -> [-5, -4, -3, -2, -1]
node .\createRange.js 9 5 -> [9, 8, 7, 6, 5]
Note the order of the values. When start is smaller than end, the order is ascending and when start is greater than end, order is descending.
*/

const args = process.argv;

let firstNumber = parseInt(args[2]);
let secondNumber = parseInt(args[3]);

const arr = [];

while (firstNumber !== secondNumber) {
    arr.push(firstNumber);
    firstNumber < secondNumber ? firstNumber++ : firstNumber--;
}

arr.push(firstNumber);
console.log(arr);