

const form = {
    gender: null,
    weight: null,
    timeSinceFirstDrink: null,
    drinkSize: null,
    alcoholByVolume: null,
    doses: null
};


function selectGender(gender) {
    form.gender = gender;
}

function checkAlcAmount() {
    if (parseFloat(document.getElementById("alcVolumeInput").value) > 100)
        document.getElementById("alcVolumeInput").value = 100;
    else if (parseFloat(document.getElementById("alcVolumeInput").value) < 0)
        document.getElementById("alcVolumeInput").value = 0;
}

function submitForm() {
    form.weight = parseFloat(document.getElementById("weightInput").value) || null;
    form.timeSinceFirstDrink = parseFloat(document.getElementById("firstDrinkH").value) || null;
    form.drinkSize = parseFloat(document.getElementById("drinkSizeSelect").value) || null;
    form.alcoholByVolume = parseFloat(document.getElementById("alcVolumeInput").value) || null;
    form.doses = parseFloat(document.getElementById("dosesInput").value) || null;

    if (Object.values(form).includes(null)) {
        console.log("insufficient data");
        return;
    }
    calculateBAC();
}

function calculateBAC() {
    const drinkSizeInLiters = form.drinkSize * form.doses;
    const grams = drinkSizeInLiters * 8 * form.alcoholByVolume;
    const burning = form.weight / 10;
    const gramsLeft = grams - (burning * form.timeSinceFirstDrink);
    const result = form.gender == "male" ? gramsLeft / (form.weight * 0.7) : gramsLeft / (form.weight * 0.6);

    document.getElementById("result").innerHTML = "Your BAC is " + Math.round(result * 100) / 100;
}

function resetForm() {
    form.gender = null;
    form.weight = null;
    form.timeSinceFirstDrink = null;
    form.drinkSize = null;
    form.alcoholByVolume = null;
    form.doses = null;

    document.getElementById("genderFemaleRadio").checked = false;
    document.getElementById("genderMaleRadio").checked = false;

    document.getElementById("weightInput").value = null;
    document.getElementById("firstDrinkH").value = null;
    document.getElementById("drinkSizeSelect").value = null;
    document.getElementById("dosesInput").value = null;
    document.getElementById("alcVolumeInput").value = null;

    document.getElementById("result").innerHTML = "Not calculated";
}