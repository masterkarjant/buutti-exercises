
const getValue = function () {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

// getValue().then(x => console.log(x));

// const someData = [getValue(), getValue()]
// Promise.all(someData).then(data => data.forEach(x => console.log(x.value)));

// await getValue()

async function valueArr() {
    const value1 = await getValue();
    const value2 = await getValue();

    return [value1.value, value2.value];
}

async function print() {
    (await valueArr()).forEach(x => console.log(x));
}
print();

/*
const someArrayOfPromises = someData.map(async (element) => {
    const resp = await axios.get(`someUrl/${element.id}`);
    return { ...element, user: resp.data };
})
*/
