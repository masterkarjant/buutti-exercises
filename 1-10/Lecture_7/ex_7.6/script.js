// import axios from "axios";
// const { default: axios } = require("axios");

// eslint-disable-next-line no-undef
axios.get("https://jsonplaceholder.typicode.com/todos/")
    .then(response => {
        const tasksArrPromise = response.data.map(async (element) => {
            // eslint-disable-next-line no-undef
            const resp = await axios.get(`https://jsonplaceholder.typicode.com/users/${element.userId}`);
            return { ...element, user: resp.data };
        });

        getMoreAccurateData(tasksArrPromise);
    })
    .catch(error => console.error(error));


function getMoreAccurateData(tasksArrPromise) {
    let dataWithoutUserIdArr;
    let dataWithUserIdArr;
    Promise.all(tasksArrPromise)
        .then(tasksData => {

            dataWithUserIdArr = tasksData;
            dataWithoutUserIdArr = tasksData.map(x => { return { ...x }; }).map(x => {
                delete x.userId;
                return x;
            });

            console.log(3, dataWithoutUserIdArr);


            const modifiedDataArr = dataWithoutUserIdArr.map(x => {
                const userObj = { name: x.user.name, username: x.user.username, email: x.user.email };
                x.user = userObj;
                return x;
            });

            console.log(4, modifiedDataArr);
            doItBetter(dataWithUserIdArr);

        })
        .catch(error => console.error(error));
}

function doItBetter(todos) {

    // eslint-disable-next-line no-undef
    axios.get("https://jsonplaceholder.typicode.com/users/")
        .then(response => {
            // 1
            const userArr = response.data;

            todos = todos.map(x => {
                return { ...x };
            });

            const modifiedTodos = todos.map(task => {
                const userIndex = userArr.findIndex(x => {
                    return x.id == task.userId;
                });
                task.user = userArr[userIndex];
                delete task.userId;
                return task;
            });



            console.log(3.1, modifiedTodos);

            const trimmedModifiedTodos = modifiedTodos.map(x => {
                const userObj = { name: x.user.name, username: x.user.username, email: x.user.email };
                x.user = userObj;
                return x;
            });

            console.log(4.1, trimmedModifiedTodos);
        })
        .catch(error => console.error(error));
}