// /*
// setTimeout(() => {
//     console.log("3");
//     setTimeout(() => {
//         console.log("2");
//         setTimeout(() => {
//             console.log("1");
//             setTimeout(() => {
//                 console.log("Go!");
//             }, 1000);
//         }, 1000);

//     }, 1000);
// }, 1000);
// */

// let index = 1;
// function play() {

// }

// new Promise((resolve, reject) => {
//     let count = 0;

//     setTimeout(() => {
//         count++;
//         console.log(count)
//     }, 1000);

//     if (count == 3)
//         resolve("Promise resolved!");
// })
//     .then((val) => {
//         console.log(val)
//         // prints resolved
//     })
//     .catch((val) => {
//         console.log(val)
//         // prints rejected
//     })

// // ESIM
// function foo() {
//     return new Promise((resolve, reject) => {
//         resolve("food?")
//     })
//     return Promise.resolve("food?");
// }
// foo().then(x => console.log(x))

// async function foo() {
//     // this returns Promise.resolve("food?"); because of the async
//     return "food?"
// }
// foo().then(x => console.log(x))

// async function f() {
//     let promise = new Promise((resolve, reject) => {
//         setTimeout(() => resolve("done!"), 1000)
//     });
//     let result = await promise; // wait until the promise resolves
//     console.log(result);
// }
// f();

// async function foo() {
//     await wait(1000)
//     return "foo"
// }

// async function wait(time) {
//     setTimeout(() => {
//         return "food"
//     }, time);
// }

// foo().then(x => console.log(x))

// await wait(1000);