import { useState } from 'react'

const getStylingObject = ({ size, color, hoverColor, isHover }) => {
    const stylingObject = {
        svg: {
            width: size + "px",
            height: size + "px",
            position: "absolute",
        },
        transition: {
            transitionDuration: '100ms',
            transitionTimingFunction: 'ease-out',
        },
        fill: (color || "black"),
    }

    if (hoverColor) {
        stylingObject.fill = isHover ? hoverColor : (color || "black")
    }

    return stylingObject
}

const SvgComponent = ({ component, size, color, hoverColor }) => {

    // HOVER
    const [isHover, setIsHover] = useState(false)
    const handleMouseEnter = () => setIsHover(true)
    const handleMouseLeave = () => setIsHover(false)

    // STYLING
    const stylingObject = getStylingObject({ size, color, hoverColor, isHover })

    // SVGS
    const svgObjects = {}

    svgObjects["trash"] = <svg
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        style={stylingObject.svg}
        viewBox="0 0 24 24">
        <path
            style={stylingObject.transition}
            fill={stylingObject.fill}
            d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
    </svg>

    return svgObjects[component]

}

export default SvgComponent