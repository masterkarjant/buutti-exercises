import { useState } from 'react'

let id = 1

function App() {

  const [toDoItemList, setToDoItemList] = useState([])
  const [inputValue, setInputValue] = useState("");

  const onInput = (event) => {
    setInputValue(event.target.value);
  }

  const addToDoItem = () => {
    setToDoItemList(toDoItemList.concat({ id: id, task: inputValue }))
    id = id + 1
    setInputValue("");
  }

  const removeTask = (id) => {
    setToDoItemList(toDoItemList.filter(task => task.id != id))
  }

  const ListOfTasks = () => {
    const listDom = toDoItemList.map((item, i) => {
      return <p key={i} onClick={() => removeTask(item.id)}><i>{item.task}</i></p>;
    })
    return listDom
  }

  return (
    <div>
      <p>Add Todo Item</p>
      <input value={inputValue} onChange={onInput}></input>
      <button onClick={addToDoItem}>Submit</button>
      <ListOfTasks />
    </div>
  )
}
export default App


// const NumberButtonList = list.map((x, i) => {
//   return <button id={i} onClick={(event) => increaseAmount(event)}>Button</button>
// })


// [number2, setNumber2] = useState(0)
// numberStates["1"] = useState(0)
// numberStates["2"] = [number2, setNumber2] = useState(0)
// numberStates["3"] = [number3, setNumber3] = useState(0)

// function increaseAmount(event) {

//   console.log(list[0])
//   // console.log(event.target.id)
//   // setNumber1(number1 = number1 + 1)
// }

// return (
// <section>
//   <h1>Hello</h1>

//   {/* <button id="0" onClick={(event) => increaseAmount(event)}>{a}</button>
//   <button id="1" onClick={(event) => increaseAmount(event)}>b</button>
//   <button id="2" onClick={(event) => increaseAmount(event)}>c</button> */}
//   {/* <div>{number1 + number2 + number3}</div> */}

// </section>
// )

// import './reset.css'

// const H1Element = ({ text }) => <h1>{text}</h1>

// const YearElement = () => {
//   const year = new Date().getFullYear()
//   return <h>It is year {year}</h2>
// }

// const ListOfNames = ({ list }) => {
//   const listDom = list.map((name, i) => {
//     return i % 2 == 1 ? <p key={i}><b>{name}</b></p> : <p key={i}><i>{name}</i></p>;
//   })
//   return listDom
// }

// function App() {

//   return (
//     <div className="App">
//       <H1Element text="Hello React!" />
//       <YearElement />
//       <ListOfNames list={["Matti", "Teppo", "Ahma", "Turpo", "Joulu Muori", "Joulu Pudvekki"]} />
//     </div>
//   )

//   // OPTIONAL
//   // const hey = <h1>Hello React!</h1>
//   // return (
//   //   [hey, <h2>It is year {new Date().getFullYear()}</h2>]
//   // )
// }

