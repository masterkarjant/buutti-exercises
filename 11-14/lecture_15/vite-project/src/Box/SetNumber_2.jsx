import { useState } from 'react'

function App() {

    const [list, setList] = useState([0, 0, 0])

    const increaseAmount = (i) => {
        setList(list.map((item, index) => index === i ? item + 1 : item));
    }

    const ListOfButtons = () => {

        const listDom = list.map((x, i) => {
            return <button key={i} id={i} onClick={() => increaseAmount(i)}>{list[i]}</button>
        })

        return listDom.concat(<h2>{list.reduce((accumulator, current) => accumulator + current, 0)}</h2>)
    }

    return (
        <div>
            <ListOfButtons />
        </div>
    )
}

export default App
