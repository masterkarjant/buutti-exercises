import { useState } from 'react'

function App() {

    const [string, setString] = useState('')
    const [muuttujaString, setMuuttujaString] = useState('')

    const onStringChange = (event) => {
        setMuuttujaString(event.target.value)
    }
    const setStateString = () => {
        setString(muuttujaString)
        setMuuttujaString("")
    }

    return (
        <div>
            <label>Your string is: {string}</label>
            <input value={muuttujaString} onChange={onStringChange}></input>
            <button onClick={setStateString}>Submit</button>
        </div>
    )
}
export default App
