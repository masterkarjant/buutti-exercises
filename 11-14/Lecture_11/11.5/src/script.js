import express from "express";

const server = express();

server.listen(3000, () => {
    console.log("Listening to port 3000");
});

server.get("/", (_req, res) => {
    res.send("Hello World");
});
