

function sequence(prev, curr) {
    sum = prev + curr;

    if (sum > 10000000) {
        console.log("that's enough")
        return;
    }

    console.log(sum);
    setTimeout(() => {
        sequence(curr, sum);
    }, 1000);
}

function startSequence() {
    console.log(0);

    setTimeout(() => {
        console.log(1);
    }, 1000);

    setTimeout(() => {
        sequence(0, 1);
    }, 2000);
}

startSequence();