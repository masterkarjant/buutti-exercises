import express from "express";
import { unknownEndpoint } from "./middleware.js";

const server = express();
server.use(express.json());



const publicNoticeBoardData = [{ id: 0, notice: "help, I'm surrounded by aliens, nyyh" }, { id: 55, notice: "no you're not, you're just probably hungry" }]

server.get("/", (_req, res) => {
    res.send(publicNoticeBoardData);
});

server.get("/:id", (req, res) => {
    const noticeIndex = publicNoticeBoardData.findIndex(x => x.id == req.params.id);

    if (noticeIndex < 0)
        return res.sendStatus(404);

    res.send(publicNoticeBoardData[noticeIndex]);
});

server.post("/", (req, res) => {
    if (!req?.body?.notice)
        return res.sendStatus(400);

    const newId = Math.max(...publicNoticeBoardData.map(x => x.id)) + 1;
    const newNotice = req.body.notice;
    publicNoticeBoardData.push({ id: newId, notice: newNotice })
    res.send("notice added");
});

server.delete("/:id", (req, res) => {
    const noticeIndex = publicNoticeBoardData.findIndex(x => x.id == req.params.id);
    if (noticeIndex < 0)
        return res.sendStatus(404);

    publicNoticeBoardData.splice(noticeIndex, 1);

    res.send("notice removed");
});

server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});

