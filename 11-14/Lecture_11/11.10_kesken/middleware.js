


export const unknownEndpoint = (_req, res, _next) => {
    res.status(404).send({ error: "Error 404, Unknown Endpoint" });
};