import express from "express";
import fetch from 'node-fetch'

const server = express();

server.listen(3000, () => {
    console.log("Listening to port 3000");
});

server.get("/", (_req, res) => {
    res.send("Hello Server One");
});

server.get("/two", (_req, res) => {
    fetch('http://server_two:3050/')
        .then(response => response.text())
        .then(response => {
            console.log(response);
            res.send(response)
        })
        .catch(error => console.error(error))
});