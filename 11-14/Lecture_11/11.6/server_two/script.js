import express from "express";

const server = express();

server.listen(3050, () => {
    console.log("Listening to port 3050");
});

server.get("/", (_req, res) => {
    res.send("Hello Server Two");
});
