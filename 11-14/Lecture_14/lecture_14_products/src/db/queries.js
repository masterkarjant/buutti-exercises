const createProductTable = `
CREATE TABLE IF NOT EXISTS "products" (
    "id" VARCHAR(36),
    "name" VARCHAR(100) NOT NULL,
    "price" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);`

const findAll = 'SELECT * FROM products';

const insertProduct = `INSERT INTO products (id, name, price) VALUES ($1,$2,$3);`

const updateProduct = `UPDATE products SET name = $2, price = $3 WHERE id = $1;`

const deleteProduct = `DELETE FROM products WHERE id = $1;`

export default { createProductTable, findAll, insertProduct, updateProduct, deleteProduct };  