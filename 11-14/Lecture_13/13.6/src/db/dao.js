import { executeQuery } from "./db.js"
import queries from "./queries.js"
import { v4 as uuidv4 } from "uuid";

const findAll = async () => {
    console.log('Requesting for all products...')
    const result = await executeQuery(queries.findAll)
    console.log(`Product ${result.rows.length} products.`)
    return result;
}

const insertProduct = async (product) => {
    const id = uuidv4();
    const params = [id, ...Object.values(product)]
    console.log(`Inserting a new product ${params[0]}...`)
    const result = await executeQuery(queries.insertProduct, params);
    console.log(`New product ${id} inserted successfully.`)
    return result;
}

const updateProduct = async (product) => {
    const params = [product.id, product.name, product.price]
    console.log(`Updating product ${params[0]}...`)
    const result = await executeQuery(queries.updateProduct, params);
    console.log(`Product ${product.id} updated successfully.`)
    return result;
}

const deleteProduct = async (id) => {
    const params = [id]
    console.log(`Deleting product with id: ${params[0]}...`)
    const result = await executeQuery(queries.deleteProduct, params);
    console.log(`Product ${id} deleted successfully.`)
    return result;
}

export default { findAll, insertProduct, updateProduct, deleteProduct };