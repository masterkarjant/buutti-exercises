import { useState } from 'react'
import { useEffect } from 'react'

function App() {
    const [seconds, setSeconds] = useState(0)

    useEffect(() => {
        const timeout = setTimeout(() => {
            setSeconds(seconds + 1)
        }, 1000);

        return () => {
            clearTimeout(timeout)
        }
    }, [seconds])

    const resetTimer = () => {
        setSeconds(0)
    }

    return (
        <div className="App">
            <h2>Timer</h2>
            {seconds}
            <button onClick={resetTimer}>Reset</button>
        </div>
    )
}

export default App
