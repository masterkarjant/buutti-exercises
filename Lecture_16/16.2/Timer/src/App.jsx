import { useState } from 'react'
import { useEffect } from 'react'

function App() {
  const [minutes, setMinutes] = useState(0)
  const [seconds, setSeconds] = useState(0)
  const [runTimer, setRunTimer] = useState(false)

  const Time = () => {
    return <h2>
      <span>{minutes.toString().length == 1 && 0}{minutes}</span>
      :
      <span>{seconds.toString().length == 1 && 0}{seconds}</span>
    </h2>
  }

  useEffect(() => {
    if (runTimer) {
      setTimeout(() => {
        setSeconds(seconds + 1);
      }, 1000);

      if (seconds == 60) {
        setMinutes(minutes + 1)
        setSeconds(0)
      }
    }
  });


  const toggleTimer = () => {
    clearTimeout();
    setRunTimer(!runTimer)
  }


  return (
    <div className="App">
      <h2>Timer</h2>
      <Time />
      <button onClick={toggleTimer}>Start/Stop</button>
      <button>Add minute</button>
      <button>Add second</button>
      <button>Stop</button>
    </div>
  )
}

export default App
