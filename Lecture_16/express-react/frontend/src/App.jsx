import { useState, useEffect } from 'react'

function App() {

  const [lyrics, setLyrics] = useState("~");

  useEffect(() => {
    initialize()
  }, [])

  const initialize = async () => {
    const url = process.env.NODE_ENV === "production" ? "./laa" : "http://localhost:3000/laa"
    const response = await fetch(url)
    const placeholder = await response.text()
    setLyrics(placeholder)
  }

  return (
    <div className="App">
      <h1>Lyrical Master Bard Sings</h1>
      <p>{lyrics}</p>
    </div>
  )
}

export default App
