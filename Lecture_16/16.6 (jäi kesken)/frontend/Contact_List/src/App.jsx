import { useState, useReducer } from 'react'
import './style.scss'

const initialListOfContacts = [{
  name: 'Bob',
  email: 'Bob@Market.cheese',
  phoneNum: '045',
  address: '',
  website: '',
  notes: 'Bob lives on the beach'
}]

function App() {
  // FORM
  const initialFormState = { name: '', email: '', phoneNum: '', address: '', website: '', notes: '' };
  const [formValue, setFormValue] = useState(initialFormState)


  // VARIOUS
  const [rightSideHeader, setRightSideHeader] = useState(null)
  const [listOfContacts, setListOfContacts] = useState(initialListOfContacts)
  const [selectedContact, setSelectedContact] = useState(null)

  const ContactList = () => {
    const listDom = listOfContacts.map((contact, i) => {
      return <li key={i} onClick={() => selectContact(contact)}>{contact.name}</li>
    })

    return <ul>{listDom}</ul>
  }

  const selectContact = (contact) => {
    console.log(contact)
    setSelectedContact(contact)
    setRightSideHeader('Contact Info')
  }

  const handleChange = (event) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value })
    console.log(formValue)
  }

  const changeRightSideView = (stateName) => () => {
    setRightSideHeader(stateName)
    setFormValue(initialFormState)
  }

  const saveNewContact = () => {
    setListOfContacts(listOfContacts.concat(formValue))
    setFormValue(initialFormState)
  }

  return (
    <div className="App">

      <main>

        <section className='leftSide'>
          <button onClick={changeRightSideView('New Contact')}>Add Contact</button>
          <ContactList />
        </section>

        <section className='rightSide'>

          <h1>{rightSideHeader}</h1>

          {rightSideHeader == 'Contact Info' && <>
            <h2>Name: {selectedContact.name}</h2>
            {selectedContact.phoneNum && <h2>Phone: {selectedContact.phoneNum}</h2>}
            {selectedContact.email && <h2>Email: {selectedContact.email}</h2>}
            {selectedContact.address && <h2>Address: {selectedContact.address}</h2>}
            {selectedContact.website && <h2>Website: {selectedContact.website}</h2>}
            {selectedContact.notes && <h2>Notes: {selectedContact.notes}</h2>}

            <button>Remove</button>
            <button>Edit</button>
            <button>Cancel</button>
          </>}

          {rightSideHeader == 'New Contact' && <>
            <label>Name</label>
            <input value={formValue.name} name="name" onChange={handleChange} type="text" />

            <label>Email Address</label>
            <input value={formValue.email} name="email" onChange={handleChange} type="text" />

            <label>Phone Number</label>
            <input value={formValue.phoneNum} name="phoneNum" onChange={handleChange} type="text" />

            <label>Address</label>
            <input value={formValue.address} name="address" onChange={handleChange} type="text" />

            <label>Website</label>
            <input value={formValue.website} name="website" onChange={handleChange} type="text" />

            <label>Notes</label>
            <textarea value={formValue.notes} name="notes" onChange={handleChange} id="" cols="5" rows="2"></textarea>

            <div className='saveCancel'>
              <button disabled={true && !formValue.name} onClick={saveNewContact}>Save</button>
              <button onClick={changeRightSideView(null)}>Cancel</button>
            </div>
          </>}
        </section>

      </main>

    </div>
  )
}

export default App

