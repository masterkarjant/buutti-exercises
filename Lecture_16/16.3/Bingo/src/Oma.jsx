import { useState } from 'react'
import './style.scss'

function App() {
  const [bingoBallsDisplay, setBingoBallsDisplay] = useState([])

  const addBingoBall = () => {
    let ballNumber = Math.floor(Math.random() * 99)
    let isIncluded = bingoBallsDisplay.find(includedBall => includedBall === ballNumber)

    console.log("attempting to add ", ballNumber, isIncluded ? 'fail' : 'success')

    while (isIncluded) {
      ballNumber = Math.floor(Math.random() * 99)
      isIncluded = bingoBallsDisplay.find(includedBall => includedBall === ballNumber)

      console.log("attempting to add ", ballNumber, isIncluded ? 'fail' : 'success')
    }

    setBingoBallsDisplay(bingoBallsDisplay.concat(ballNumber))
  }

  const BingoBalls = () => {
    console.log(typeof bingoBallsDisplay)
    console.log(bingoBallsDisplay)
    const ballsDom = bingoBallsDisplay.map((ball, i) => {
      return <div key={i} className='ball'>{ball}</div>
    })
    return ballsDom
  }

  return (
    <div className="App">
      <button onClick={addBingoBall}>+</button>
      <BingoBalls />
    </div>
  )
}
export default App
