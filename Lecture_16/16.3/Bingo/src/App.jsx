import { useEffect, useState } from "react";

const potentialNumbers = Array(100).fill(undefined).map((_n, i) => i + 1);

function App() {
    const [numbers, setNumbers] = useState([])

    const addNumber = () => {
        if (!potentialNumbers.length)
            return

        const randomIndex = Math.floor(Math.random() * potentialNumbers.length)
        const removed = potentialNumbers.splice(randomIndex, 1)
        setNumbers(numbers.concat(removed[0]))
        console.log(potentialNumbers)
    }

    return (
        <div>
            {numbers.map(number => {
                return <div key={number}>{number}</div>
            })}
            <button onClick={addNumber}>+</button>
        </div>
    )
}

export default App