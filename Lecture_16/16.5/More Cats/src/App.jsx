import { useState } from 'react'

const imageUrl = "https://cataas.com/cat";
const imageUrlForText = "https://cataas.com/cat/says/";

function App() {
  const [img, setImg] = useState("https://cataas.com/cat")
  const [text, setText] = useState("")

  const fetchImage = async () => {
    let targetUrl = imageUrl;
    if (text.length) targetUrl = imageUrlForText + text

    const res = await fetch(targetUrl);
    const imageBlob = await res.blob();
    const imageObjectURL = URL.createObjectURL(imageBlob);
    setImg(imageObjectURL);
  };

  const onTextInput = (event) => {
    setText(event.target.value)
  }

  const changeImg = () => {
    setImg("https://cataas.com/cat")
  }

  return (
    <div className="App">
      <h1>Render Random Cat</h1>
      <input value={text} onChange={onTextInput} placeholder='add text to image' type="text" />
      <button onClick={fetchImage}>Generate Meow</button>
      <div style={{ margin: '20px 0' }}>
        <img src={img || imageUrl} alt="" />
      </div>

    </div>
  )
}

export default App
