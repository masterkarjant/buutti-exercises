import { useState } from 'react'
import './style.scss'

const initialFormState = {
  type: null,
  message: "",
  name: "",
  email: ""
}

function App() {
  const [formValue, setFormValue] = useState(initialFormState)

  const selectType = (event) => {
    setFormValue({ ...formValue, type: event.target.value })
  }

  const updateMessageInput = (event) => {
    setFormValue({ ...formValue, message: event.target.value })
  }

  const updateNameInput = (event) => {
    setFormValue({ ...formValue, name: event.target.value })
  }

  const updateEmailInput = (event) => {
    setFormValue({ ...formValue, email: event.target.value })
  }

  const submitForm = () => {
    console.log(formValue)
  }

  return (
    <div className="App">

      <div className="form">
        <section>
          <legend>Type</legend>
          <div>
            <input type="radio" id="feedback" name="typeSelect" onChange={selectType} value="feedback" checked={formValue.type === 'feedback'} />
            <label htmlFor="feedback">Feedback</label>
          </div>

          <div>
            <input type="radio" id="suggestion" name="typeSelect" onChange={selectType} value="suggestion" checked={formValue.type === 'suggestion'} />
            <label htmlFor="suggestion">Suggestion</label>
          </div>

          <div>
            <input type="radio" id="question" name="typeSelect" onChange={selectType} value="question" checked={formValue.type === 'question'} />
            <label htmlFor="question">Question</label>
          </div>

        </section>

        <section>
          <legend>Message</legend>
          <textarea name="" id="" cols="30" rows="10" onChange={updateMessageInput}></textarea>
        </section>

        <section className='formContactSection'>
          <legend>Contact info</legend>
          <label htmlFor="name">Name</label>
          <input id="name" onChange={updateNameInput}></input>
          <label htmlFor="email">Email</label>
          <input id="email" onChange={updateEmailInput}></input>
        </section>

        <button onClick={submitForm}>Submit</button>

      </div>
    </div>
  )
}

export default App
