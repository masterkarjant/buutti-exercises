import songs from './songs.js'
import { useLoaderData } from 'react-router-dom'

const throw404Error = () => {
    const error = new Error('404 - Not Found')
    error.status = 404
    throw error
}

export function loader({ params }) {
    const targetSong = songs.find(song => song.id === Number(params.id))
    if (!targetSong) throw404Error()
    return targetSong
}

function Song() {

    const { title, lyrics } = useLoaderData()
    const paragraphsArr = lyrics.split(/\d/)

    return (
        <div className='song'>
            <h2>{title}</h2>
            {paragraphsArr.map((paragraph, i) => paragraph.length > 1 ? <p key={i + 1}>{paragraphsArr.length > 1 && i}{paragraph}</p> : null)}
        </div>

    )
}

export default Song