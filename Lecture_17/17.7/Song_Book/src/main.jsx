import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import SongBook from './SongBook'
import ErrorPage from './ErrorPage'
import Song, { loader as songLoader } from './Song'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import './style.scss'


const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />,

    children:
      [{
        path: 'songbook',
        element: <SongBook />,

        children:
          [{
            path: ':id',
            element: <Song />,
            loader: songLoader
          }],
      }],
  },
])


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
