import { useState } from 'react'
import { Link, Outlet } from 'react-router-dom'
import songs from './songs.js'

function SongBook() {
    const [count, setCount] = useState(0)

    return (
        <main className='songBook'>
            <div className='songList'>
                {songs.map(song => <Link key={song.id} to={song.id.toString()}>{song.title}</Link>)}
            </div>

            <Outlet />
        </main>

    )
}

export default SongBook