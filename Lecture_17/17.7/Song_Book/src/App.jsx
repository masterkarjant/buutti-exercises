import { useState, useEffect } from 'react'
import { Link, Outlet, useNavigate } from 'react-router-dom'

function App() {

  useEffect(() => {

    console.log("Hello")


  }, []);

  return (
    <div className="App">

      <main>
        <nav className='mainNav'>
          <Link to={'songbook'}>Song Book</Link>
        </nav>
      </main>

      <Outlet />

    </div>
  )
}

export default App
