import { useLoaderData } from 'react-router-dom'

const throw404Error = () => {
    const error = new Error('404 - Not Found')
    error.status = 404
    throw error
}

export async function loader({ params }) {
    const response = await fetch('http://localhost:3000/songs/' + params.id)
    const responseJson = await response.json()

    if (responseJson.error) { console.log("Error:", responseJson.error); throw404Error() }

    return responseJson
}

function Song() {

    const { title, lyrics } = useLoaderData()
    const paragraphsArr = lyrics.split(/\d/)

    return (
        <div className='song'>
            <h2>{title}</h2>
            {paragraphsArr.map((paragraph, i) => paragraph.length > 1 ? <p key={i}>{paragraphsArr.length > 1 && i}{paragraph}</p> : null)}
        </div>

    )
}

export default Song