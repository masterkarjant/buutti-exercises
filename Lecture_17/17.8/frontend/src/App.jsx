import { Link, Outlet } from 'react-router-dom'

function App() {

  return (
    <div className="App">

      <main>
        <nav className='mainNav'>
          <Link to={'songbook'}>Song Book</Link>
        </nav>
      </main>

      <Outlet />

    </div>
  )
}

export default App
