import { useState, useEffect } from 'react'
import { Link, Outlet } from 'react-router-dom'

function SongBook() {

    const [songs, setSongs] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:3000/songs')
        const responseJson = await response.json()
        setSongs(responseJson.data)
    }

    useEffect(() => {
        getData();
    }, []);

    return (
        <main className='songBook'>
            <div className='songList'>
                {songs.length > 0 && songs.map(song => <Link key={song.id} to={song.id.toString()}>{song.title}</Link>)}
            </div>

            <Outlet />
        </main>

    )
}

export default SongBook