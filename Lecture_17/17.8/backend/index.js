import express from "express"
import cors from "cors"

import path from 'path'
import url from 'url'

import songs from "./songs.js"

const server = express()
server.use(cors())

// Absolute directory is required for the sendFile method
const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url))
const distDirectory = path.resolve(currentDirectory, './dist')

server.get("/songs", (_req, res) => {
    res.send({ data: songs.map(song => { return { title: song.title, id: song.id } }) })
})

server.get("/songs/:id", (req, res) => {
    const targetSong = songs.find(song => song.id == req.params.id);

    if (!targetSong)
        res.send({ error: "no song with that id available" })

    res.send(targetSong)
})

server.use('/', express.static(distDirectory))

server.listen(3000, () => {
    console.log("listening to 3000")
})