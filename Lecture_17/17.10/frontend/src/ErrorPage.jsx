import { useRouteError, Link } from 'react-router-dom'

function ErrorPage() {
    const error = useRouteError()

    if (error.status === 404) return (
        <div className='ErrorPage'>
            <h1>404 - Not Found</h1>
            <p>The requested site does not exist</p>
            <Link to={'/songbook'}>Go to Song Book</Link>
        </div>
    )

    console.error(error)
    return (
        <div className='ErrorPage'>
            <h1>An Unexpected error happened!</h1>
            <p>
                {error.statusText || error.message}
            </p>
        </div>
    )
}

export default ErrorPage