import { useState } from 'react'

function App() {
  const [randomColor1, setRandomColor1] = useState('#22c1c3')
  const [randomColor2, setRandomColor2] = useState('#fdbb2d')
  const [deg, setDeg] = useState(90)

  const boxStyle = {
    width: "200px",
    height: "200px",
    background: "linear-gradient(" + deg + "deg, " + randomColor1 + " 0%, " + randomColor2 + " 100%)",
    textAlign: "center"

  }

  const textStyle = {
    color: "white",
    lineHeight: "200px",
    userSelect: "none"
  }

  const changeColor = () => {
    const randomColor1 = "#" + Math.floor(Math.random() * 16777215).toString(16);
    const randomColor2 = "#" + Math.floor(Math.random() * 16777215).toString(16);
    setRandomColor1(randomColor1)
    setRandomColor2(randomColor2)

    setTimeout(() => {
      changeColor()
    }, 50);
  }


  return (
    <div className="App">


      <h1>Hexabox Uh</h1>
      <div onClick={changeColor} style={boxStyle}>
        <span style={textStyle}>{randomColor1} / {randomColor2}</span>
      </div>
    </div>
  )
}

export default App
