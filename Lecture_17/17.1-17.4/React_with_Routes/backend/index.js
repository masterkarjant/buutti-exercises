import express from 'express'
import url from 'url'
import path from 'path'

const server = express()

// Absolute directory is required for the sendFile method
const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url))
const distDirectory = path.resolve(currentDirectory, '../frontend/dist')

server.use('/', express.static(distDirectory))

// Redirect all other requests to React Router
server.get('*', (_req, res) => {
    res.sendFile('index.html', { root: distDirectory })
})

// OPTIONAL WAY
// server.use((req, res) => {
//     res.sendFile('index.html', { root: distDirectory })
// })

server.listen(3000)
