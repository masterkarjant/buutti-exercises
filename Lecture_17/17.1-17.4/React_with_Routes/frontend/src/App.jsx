import { useState } from 'react'
import { Link } from 'react-router-dom'

function App() {

  return (
    <div className="App">
      <p>
        <Link to={'/admin'}>Go to Admin</Link>
      </p>
      <p>
        <Link to={'/contacts'}>Go to Contacts</Link>
      </p>

    </div>
  )
}

export default App
