import { useState } from 'react'
import { Link, Outlet } from 'react-router-dom'

const contactList = [
    { id: 1, name: 'Abu', phoneNum: '123', email: 'at' },
    { id: 2, name: 'Mubu', phoneNum: '456', email: 'dot' },
    { id: 3, name: 'Sabi', phoneNum: '789', email: 'boat' }
]

function Contacts() {

    return <div>
        <nav>
            {contactList.map((contact, i) => {
                return <Link className='contactLink' key={i} to={contact.id.toString()}>{contact.name}</Link>
            })}
        </nav>

        <Outlet />
    </div>
}

export default Contacts
