import './style.css'
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import Contacts from './Contacts'
import Contact, { loader as contactLoader } from './Contact'
import Admin from './Admin'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import ErrorPage from './ErrorPage'

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />
  },
  {
    path: '/contacts',
    element: <Contacts />,
    children: [{
      path: ':id',
      element: <Contact />,
      loader: contactLoader
    }],
  },
  {
    path: '/admin',
    element: <Admin />,
  },
])


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
