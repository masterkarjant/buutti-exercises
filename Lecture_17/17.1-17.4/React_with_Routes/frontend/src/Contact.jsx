import { useLoaderData } from 'react-router-dom'

export function loader({ params }) {
    const targetContact = contactList.find(contact => contact.id === Number(params.id))

    return targetContact ? targetContact : "No contact with that id exist"
}

const contactList = [
    { id: 1, name: 'Abu', phoneNum: '123', email: 'at' },
    { id: 2, name: 'Mubu', phoneNum: '456', email: 'dot' },
    { id: 3, name: 'Sabi', phoneNum: '789', email: 'boat' }
]

function Contact() {

    const { id, name, phoneNum, email } = useLoaderData()

    let contact = <div>
        <p>Id: {id}</p>
        <p>Name: {name}</p>
        <p>PhoneNum: {phoneNum}</p>
        <p>Email: {email}</p>
    </div>

    if (!id)
        contact = <div>No contact with that id exist</div>

    return (
        <div className="App">
            <h1>Contacts</h1>
            {contact}
        </div>
    )
}

export default Contact
