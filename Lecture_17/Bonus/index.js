import express from 'express'
import { executeQuery } from "./db.js"

const server = express()
server.use(express.json());

const findAll = async () => {
    const result = await executeQuery('SELECT * FROM messages')
    console.log(`Product ${result.rows.length} products.`)
    return result;
}

server.get('/', (req, res) => {
    res.send("we are here")
})

server.get('/message', async (req, res) => {
    const result = await findAll();
    console.log(result)
    res.send(result)
})

server.listen(3000, () => {
    console.log("Listening to port 3000");
});