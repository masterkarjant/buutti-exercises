import pg from 'pg';

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;

export const pool = new pg.Pool({
    host: 'jsc-lecture-17-demo.postgres.database.azure.com',
    port: 3000,
    user: 'jscadmin@jsc-lecture-17-demo',
    password: 'jscpass1!',
    database: 'postgres',
    ssl: true
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result;
    } catch (error) {
        console.error(error.stack);
        error.name = 'dbError';
        throw error
    } finally {
        client.release();
    }
}

export const createProductTable = async () => {
    await executeQuery(queries.createProductTable)
    console.log('Product table initialization successful')
}

export default { executeQuery, pool }